﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;
using System.Text;

namespace notre_bibliotheque
{
    /// <summary>
    /// Le gestionaire de comptes permet de gerrer les comptes, il sert d'interface entre le model et l'application
    /// </summary>
    public class GestionaireDeComptes : Gestionaire, INotifyPropertyChanged
    {
        public bool IsSomeoneConnected => !(ItemsComptes.ItemCourant == null);

        // ItemsComptes contien, entre autre, la liste des comptes
        public Items ItemsComptes { get; set;}
        public GestionaireDeComptes()
        {
            ItemsComptes = new Items();
        }
        public GestionaireDeComptes(Items its)
        {
            ItemsComptes = its;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        ///     Permet de verrifier si un nouveau compte est acceptable
        /// </summary>
        /// <param name="id">Identrifiant du nouveau compte</param>
        /// <param name="mdp1">Mot de passe du nouveau compte</param>
        /// <param name="mdp2">Verrification du mot de passe</param>
        /// <param name="admin">Autorise l'accès au mode administrateur</param>
        /// <returns>Si le nouveau compte est validé, il est retourné</returns>
        public Compte VerfierCreationCompte(string id, string mdp1, string mdp2,bool admin)
        {
            foreach (Compte c in ItemsComptes.LesItems)
            {
                if (id == c.Identifiant)
                {
                    throw new Exception("Ce nom d'utilisateur existe déjà");
                }
            }
            if (mdp1 != mdp2)
            {
                throw new Exception("Les mots de passes sont différents");
            }
            Compte tmp = new Compte(id, mdp1, admin);
            return tmp;
        }

        /// <summary>
        /// Permet de charger les comptes grace à la proprieté Persistance de Gestionaire
        /// </summary>
        public void ChargerLesComptes()
        {
            ItemsComptes.LesItems.Clear();
            var lesComptesChargés = Persistance?.ChargerLesDonnées()["Comptes"];
            if (lesComptesChargés == null)
            {
                throw new Exception("PB : chargement imposible");
            }

            foreach (Compte it in lesComptesChargés)
            {
                ItemsComptes.Ajouter(it);
            }
        }
        public void onPropertyChanged(string prop)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }
    }

}
