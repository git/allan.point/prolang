﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;

namespace notre_bibliotheque
{
    /// <summary>
    /// La classe Gestionnaire permet de creer des gestionnaire qui pouront implémenter une persistance.
    /// </summary>
    public abstract class Gestionaire
    {
        public static IPercistance Persistance { get; set; }
    }
}
