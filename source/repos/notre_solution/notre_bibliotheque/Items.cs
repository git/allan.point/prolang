﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml.Serialization;
using System.Linq;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Diagnostics;
using System.Collections.ObjectModel;

namespace notre_bibliotheque
{
    /// <summary>
    /// Classe qui sert de point de sérialization.
    /// </summary>
    public class Items : IEquatable<Items>, INotifyPropertyChanged
    {
        private ValeurTri filtre;
        public ValeurTri Filtre
        {
            get => filtre;
            set
            {
                filtre = value;
                OnPropertyChanged("LesItems");
            }
        }
        
        public Item ItemCourant   { get; set; }
        public Items ItemsDeLAutreType { get; set; }
        private IList<Item> lesItems;

        public event PropertyChangedEventHandler PropertyChanged;

        // LesItems est une propriété calculé en fonction du Filtre de tri
        public IList<Item> LesItems {
            get
            {
                try
                 {
                     switch(Filtre)
                     {
                         case ValeurTri.Date:
                             return lesItems.OrderBy(it => (it as Langage).DateDuLangage).ToList();
                         case ValeurTri.Favoris:
                            if (ItemsDeLAutreType.ItemCourant == null) return new List<Item>();
                            IList<Item> tmp = new List<Item>();
                            foreach(Item it in (ItemsDeLAutreType.ItemCourant as Compte).LesLangagesFavoris.OrderBy(lang => (lang as Langage).Nom).ToList())
                            {
                                tmp.Add(it);
                            }
                             return tmp;
                         case ValeurTri.Génération:
                             return lesItems.OrderBy(it => (it as Langage).Generation).ToList();
                         case ValeurTri.Historique:
                             if (ItemsDeLAutreType.ItemCourant == null) return new List<Item>();
                             List<Item> historique = new List<Item>();
                             foreach(Item lang in (ItemsDeLAutreType.ItemCourant as Compte).HistoriqueDuCompte)
                             {
                                 historique.Add(lang);
                             }
                             historique.Reverse();
                             return historique;
                         default:
                             return lesItems.OrderBy(it => (it as Langage).Nom).ToList();
                     }
                 }
                 catch (InvalidCastException)
                 {
                     return lesItems;
                 }
                 catch(NullReferenceException)
                 {
                     return lesItems;
                 }
            }
                
            private set
            {
                lesItems = value;
            }
        }

        public Items(IList<Item> items)
        {
            Filtre = ValeurTri.Nom;
            LesItems = new ObservableCollection<Item>(items);
        }

        public Items()
        {
            Filtre = ValeurTri.Nom;
            LesItems = new ObservableCollection<Item>();
        }

        /// <summary>
        /// Permet de supprimer un Item de LesItems (la liste d'item (Compte ou Langage))
        /// </summary>
        /// <param name="it">Item a supprimer</param>
        public void Supprimer(Item it)
        {
            if (lesItems.Remove(it) == false)
            {
                throw new ArgumentException($"{it} n'existe pas :/");
            }
            if(ItemsDeLAutreType.ItemCourant == it)
            {
                ItemsDeLAutreType.ItemCourant = null;
            }
            if(ItemsDeLAutreType.ItemCourant == it)
            {
                ItemsDeLAutreType.ItemCourant = null;
            }
            //quand un item est supprimé, la propriété LesItems est notifiée
            OnPropertyChanged("LesItems");
        }

        /// <summary>
        /// Permet d'ajouter un Item dans la liste d'Item (LesItems)
        /// </summary>
        /// <param name="it">Item à ajouter</param>
        public void Ajouter(Item it)
        {
            if (lesItems.Contains(it) == true)
            {
                throw new Exception("L'item existe déja");
            }
            lesItems.Add(it);
            OnPropertyChanged("LesItems");
        }

        /// <summary>
        /// Vérifie qu'un Item est dans la liste des Item (LesItems)
        /// </summary>
        /// <param name="it">Item à tester</param>
        /// <returns></returns>
        public bool Exists(Item it)
        {
            return lesItems.Contains(it);
        }

        public bool Equals(Items other)
        {
            ValeurTri ancienFiltreOther = other.Filtre;
            other.Filtre = ValeurTri.Nom;

            bool valeurARetourner = lesItems.Equals(other.LesItems);
            other.Filtre = ancienFiltreOther;
            return valeurARetourner;
        }

        public override bool Equals(object obj)
        {
            if (obj is null) return false;
            if (obj == this) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals(obj as Items);
        }

        /// <summary>
        /// Permet de notifier le changement de valeur d'une propriété
        /// </summary>
        /// <param name="prop">nom de la propriété qui change de valeur</param>
        /// <remarks>Si on appel OnPropertyChanged() sans arguments, alors ce sera la propriété qui est en train de mutter qui sera automatiquement passer en argument</remarks>
        private void OnPropertyChanged([CallerMemberName]string prop = null)
        {
            Debug.WriteLine(prop);
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
