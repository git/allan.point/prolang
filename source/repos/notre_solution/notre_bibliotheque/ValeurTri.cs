﻿using System;
using System.Collections.Generic;
using System.Text;

namespace notre_bibliotheque
{
    /// <summary>
    /// cette enum regroupe les valeurs possibles pour trier la liste des langages
    /// la liste sera alors triable par nom, date de création et par génération,
    /// et pourra être filtré par les langages present dans les listes favoris et historique du compte 
    /// actuellement connecté
    /// </summary>
    public enum ValeurTri
    {
        Nom,
        Date,
        Génération,
        Favoris,
        Historique,
    }
}
