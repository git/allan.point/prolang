﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace notre_bibliotheque
{
    /// <summary>
    /// Ce gestionaire de langage permet de gerrer les langages.
    /// </summary>
    public class GestionaireDeLangages : Gestionaire, INotifyPropertyChanged
    {

        private Items itemsLangages;
        // ItemsLangages conntient, entre autre, la liste des langages consutables
        // et notifie ses abbonés lors ce qu'il change
        public Items ItemsLangages
        {
            get => itemsLangages;
            set
            {
                itemsLangages = value;
                OnPropertyChange();
            }
        }
        public bool IsASelectedLanguage
        {
            get => ItemsLangages.ItemCourant != null;
        }

        public GestionaireDeLangages()
        {
            ItemsLangages = new Items();
        }

        public GestionaireDeLangages(Items its)
        {
            ItemsLangages = its;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public Langage VerifierCreationLangage(string nom, int date, IList<string> auteurs, string doc, string logo, string exemple, IList<string> logiciels, string utilite, IList<string> paradigmes, int gen)
        {
            Langage tmp = new Langage(nom, date, auteurs, doc, logo, exemple, logiciels, utilite, paradigmes, gen);
            if (ItemsLangages.Exists(tmp))
            {
                throw new Exception("Ce langage existe déjà");
            }
            return tmp;
        }

        public void OnPropertyChange([CallerMemberName] string prop = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }

        /// <summary>
        /// Permet de charger les langages depuis la propriété Persistance de Gestionaire
        /// </summary>
        public void ChargerLesLangages()
        {
            itemsLangages.LesItems.Clear();
            var lesLangagesChargés = Persistance?.ChargerLesDonnées()["Langages"];
            if(lesLangagesChargés == null)
            {
                throw new Exception("PB : chargement impossible");
            }

            foreach(Item it in lesLangagesChargés)
            {
                ItemsLangages.Ajouter(it);
            }
        }
    }
}

