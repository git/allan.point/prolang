﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;
using System.Text;

namespace notre_bibliotheque
{
    /// <summary>
    /// cette classe va permettre à l'utilisateur de voir toutes les informations concernant un langage
    /// elle implémente l'interface Item, IEquatable<Langage> (pour le protocole d'égalité) ainsi que 
    /// INotifyPropertyChanged
    /// </summary>
    
    [DataContract]
    public class Langage : Item, IEquatable<Langage>, INotifyPropertyChanged
    {
        //un langage a un nom de type string
        private string nom;
        [DataMember]
        public string Nom
        {
            get => nom;
            set
            {
                if (String.IsNullOrEmpty(value.Trim()))
                {
                    throw new Exception("Le nom doit être renseigné");
                }
                nom = value;
            }
        }

        //un langage a une date de création de type int (juste l'année)
        private int dateDuLangage;
        [DataMember]
        public int DateDuLangage
        {
            get => dateDuLangage;
            set
            {
                if(value <= 1900)
                {
                    throw new Exception("La date de création doit être renseigné");
                }
                dateDuLangage = value;
            }
        }

        //un langage a une collection d'auteurs de type IList<string>
        private IList<string> lesAuteurs;
        [DataMember]
        public IList<string> LesAuteurs
        {
            get => lesAuteurs;
            set
            {
                if(value == null)
                {
                    throw new Exception("Les auteurs ne doivent pas être null");
                }
                if(value.Count == 0)
                {
                    throw new Exception("Il doit y avoir au moins un auteur");
                }
                foreach(string auteur in value)
                {
                    if (String.IsNullOrEmpty(auteur.Trim()))
                    {
                        throw new Exception("Le nom de l'auteur doit être renseigné");
                    }
                    
                }
                lesAuteurs = value;
            }
        }

        //un langage a un lien vers sa documentation de type string
        private string documentation;
        [DataMember]
        public string Documentation
        {
            get => documentation;
            set 
            {
                if (String.IsNullOrEmpty(value.Trim()))
                {
                    throw new Exception("La documentation doit être renseigné");
                }
                documentation = value;
            }
        }

        //un langage a un chemin pour son logo de type string
        private string cheminDuLogo;
        [DataMember]
        public string CheminDuLogo 
        { 
            get => cheminDuLogo;
            set 
            {
                if (String.IsNullOrEmpty(value.Trim()))
                {
                    throw new Exception("le chemin du logo doit être renseigné");
                }
                cheminDuLogo = value;
            }
        }

        //un langage a une collection de logiciels connus de type IList<string>
        private IList<string> logicielsConnus;
        [DataMember]
        public IList<string> LogicielsConus 
        { 
            get => logicielsConnus;
            set
            {
                if (value == null)
                {
                    throw new Exception("Les logiciels ne doivent pas être null");
                }
                if (value.Count == 0)
                {
                    throw new Exception("Il doit y avoir au moins un logiciel");
                }
                foreach (string logiciel in value)
                {
                    if (String.IsNullOrEmpty(logiciel.Trim()))
                    {
                        throw new Exception("Le nom du logiciel doit être renseigné");
                    }

                }
                logicielsConnus = value;
            }
        }

        //un langage a une description de son utilité de type string
        private string utilitéDuLangage;
        [DataMember]
        public string UtilitéDuLangage
        {
            get => utilitéDuLangage;
            set
            {
                if (String.IsNullOrEmpty(value.Trim()))
                {
                    throw new Exception("l'utilité du langage doit être renseigné");
                }
                utilitéDuLangage = value;
            }
        }

        //un langage a un exemple de code "Hello World" de type string
        private string exempleDeCode;
        [DataMember]
        public string ExempleDeCode
        {
            get => exempleDeCode;
            set
            {
                if (String.IsNullOrEmpty(value.Trim()))
                {
                    throw new Exception("l'exemple de code doit être renseigné");
                }
                exempleDeCode = value;
            }
        }

        //un langage a une collection de paradigmes de type IList<string>
        private IList<string> lesParadigmes;
        [DataMember]
        public IList<string> LesParadigmes
        {
            get => lesParadigmes;
            set
            {
                if (value == null)
                {
                    throw new Exception("Les paradigmes ne doivent pas être null");
                }
                if (value.Count == 0)
                {
                    throw new Exception("Il doit y avoir au moins un paradigme");
                }
                foreach (string parag in value)
                {
                    if (String.IsNullOrEmpty(parag.Trim()))
                    {
                        throw new Exception("Le nom du paradigme doit être renseigné");
                    }

                }
                lesParadigmes = value;
            }
        }

        //un langage a une génération de type int
        int generation;
        [DataMember]
        public int Generation
        {
            get => generation;
            set 
            {
                if (value <= 0)
                {
                    throw new ArgumentException("La génération doit être suppérieur a 0");
                }
                generation = value;

            }
        }

        //la propriété DansLesFavoris de type bool indique si le langage sélectionné par l'utilisateur
        //est actuellement dans la liste des favoris du compte de l'utilisateur (si celui-ci est connecté)
        //elle invoque l'événeiment PropertyChanged quand sa valeur est set
        private bool dansLesFavoris;
        public bool DansLesFavoris 
        {
            get => dansLesFavoris;
            set
            {
                dansLesFavoris = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("DansLesFavoris"));
            } 
        }

        //l'événement PropertyChanged est utilisé par la propriété DansLesFavoris, pour notifier à l'ensemble
        //du programme quand cette propriété est set
        public event PropertyChangedEventHandler PropertyChanged;

        //cette classe a deux constructeurs

            /// <summary>
            /// ce premier constructeur ne prend aucun parametres, les propriétés du langages ne sont alors pas set
            /// (ce constructeur a surtout été utile pour les tests)
            /// </summary>
            public Langage() { }

            /// <summary>
            /// ce second constructeur prend en parametres toutes les informations necessaires pour créer un langage
            /// les propriétés du langage vont alors etre set par toutes ses valeurs données en parametre
            /// (sauf pour DansLesFavoris qui est mis à false quand le langage est créé)
            /// </summary>
            /// <param name="nom"></param>
            /// <param name="date"></param>
            /// <param name="auteurs"></param>
            /// <param name="doc"></param>
            /// <param name="logo"></param>
            /// <param name="exemple"></param>
            /// <param name="logiciels"></param>
            /// <param name="utilite"></param>
            /// <param name="paradigmes"></param>
            /// <param name="generation"></param>
            public Langage(string nom, int date, IList<string> auteurs, string doc, string logo, string exemple, IList<string> logiciels, string utilite, IList<string> paradigmes, int generation)
            {
                Nom = nom;
                DateDuLangage = date;
                LesAuteurs = auteurs;
                Documentation = doc;
                CheminDuLogo = logo;
                LogicielsConus = logiciels;
                UtilitéDuLangage = utilite;
                ExempleDeCode = exemple;
                LesParadigmes = paradigmes;
                Generation = generation;
                DansLesFavoris = false;

            }

        /// <summary>
        /// la methode ToString permet d'afficher les différentes informations d'un langage
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            string affiche_auteurs = null;
            string affiche_logiciels = null;
            string affiche_paradigme = "";

            foreach(string auteur in LesAuteurs)
            {
                affiche_auteurs += $"{auteur} \n";
            }

            foreach(string logiciel in LogicielsConus)
            {
                affiche_logiciels += $"{logiciel} \n";
            }

            foreach(string paradigme in LesParadigmes)
            {
                affiche_paradigme += $"{paradigme} \n";
            }

            return $"Nom du langage = {Nom} \n Date de création = {DateDuLangage} \n Auteurs = {affiche_auteurs} \n Documentation = {Documentation} \n " +
                $"Logo = {CheminDuLogo} \n Paradigmes = {affiche_paradigme}\n Logiciels Connus = {affiche_logiciels} \n Utilité du langage = {UtilitéDuLangage} \n Exemple de code (Hello World) = {ExempleDeCode}\n Génération = {Generation}";
        }

        /// <summary>
        /// ces trois methodes servent à redéfinir protocole d'égalité
        /// deux langages sont les mêmes si ils ont le même nom
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {
            if (obj == null) return false;
            if (this == obj) return true;
            if (this.GetType() != obj.GetType()) return false;
            return Equals(obj as Langage);
        }

        public bool Equals(Langage other)
        {
            return this.Nom == other.Nom;
        }

        public override int GetHashCode()
        {
            return ((Nom.Length + DateDuLangage) * LesAuteurs.Count) / 3;
        }
    }
}
