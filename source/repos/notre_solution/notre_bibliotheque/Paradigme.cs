﻿using System;
using System.Collections.Generic;

namespace notre_bibliotheque
{
    public class Paradigme : IEquatable<Paradigme>
    {
        public string Nom { get; private set; }
        public static IList<Paradigme> LesParadigmes { get; set; }

        public Paradigme(string nom)
        {
            Nom = nom;
        }

        public static bool IsParadigmeExists(Paradigme p)
        {
            return LesParadigmes.Contains(p);
        }

        public static bool IsParadigmeExists(string p)
        {
            foreach(Paradigme paradigme in LesParadigmes)
            {
                if(p == paradigme.Nom)
                {
                    return true;
                }
            }
            return false;
        }

        public override string ToString()
        {
            return Nom;
        }

        public override bool Equals(object obj)
        {
            if (obj == null) return false;
            if (obj == this) return false;
            if (GetType() != obj.GetType()) return false;
            return Equals(obj as Paradigme);
        }
        public bool Equals(Paradigme other)
        {
            return Nom == other.Nom;
        }
    }
}
