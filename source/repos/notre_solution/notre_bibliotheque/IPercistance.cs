﻿using System;
using System.Collections.Generic;
using System.Text;

namespace notre_bibliotheque
{
    /// <summary>
    /// cette interface va permettre la persistance de toutes les données (langages et comptes)
    /// </summary>
    public interface IPercistance
    {
        /// <summary>
        /// cette methode sert à charger les données; 
        /// elle crée un dictionnaire avec une clé de type string et une valeur de type IEnumerable<Item>,
        /// elle ajoute deux éléments dans le dictionnaire : une clé Langages avec en valeur la liste LesLangages de Data, 
        /// et une clé Comptes avec en valeur la liste LesComptes de Data
        /// </summary>
        /// <returns> elle retourne le dictionnaire </returns>
        Dictionary<string, IEnumerable<Item>> ChargerLesDonnées();

        /// <summary>
        /// cette méthode sert à sauvegarder les données;
        /// elle prend deux parametres, une collection IEnumérable<Langage> et une collection IEnumerable<Compte>
        /// ces collections sont les listes de langages et comptes à sauvegarder
        /// </summary>
        /// <param name="langagesÀSauvegarder"></param>
        /// <param name="comptesÀSauvegarder"></param>
        void SauvegarderLesDonnées(IEnumerable<Langage> lesLangages, IEnumerable<Compte> lesComptes);
    }
}
