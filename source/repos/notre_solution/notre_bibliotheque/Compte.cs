﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;
using System.Text;

namespace notre_bibliotheque
{
    /// <summary>
    /// cette classe va permettre à l'utilisateur de se connecter pour avoir acces à des fonctions suplémentaires
    /// elle implémente l'interface Item ainsi que IEquatable<Compte> (pour le protocole d'égalité)
    /// </summary>
    
    [DataContract]
    public class Compte : Item, IEquatable<Compte>
    {
        //un compte a un identifiant de type string
        [DataMember]
        public string Identifiant { get; private set; }

        //un compte a un mot de passe de type string
        private string motDePasse;


        [DataMember]
        public string MotDePasse {
            get
            {
                return motDePasse;
            }
            set
            {
                 motDePasse = value;
            }
        }

        //la propriété EstAdmin de type bool indique si l'utilisateur à le droit d'accès au mode administrateur
        [DataMember]
        public bool EstAdmin { get; private set; }

        //un compte a une collection LesLangagesFavoris de type IList<Langage> ; cette propriété regroupe
        //tous les langages que l'utilisateur a ajouté en tant que favoris
        [DataMember]
        public IList<Langage> LesLangagesFavoris { get; private set; }

        //un compte a une collection HistoriqueDuCompte de type IList<Langage> ; cette propriété regroupe
        //tous les langages que l'utilisateur a consulté
        [DataMember]
        public IList<Langage> HistoriqueDuCompte { get; private set; }

        //cette classe a deux constructeurs

            /// <summary>
            /// ce premier constructeur prend en parametre un identifiant et un mot de passe de type string,
            /// ainsi qu'un bool pour indiquer si le compte à le droit d'acces au mode administrateur
            /// </summary>
            /// <param name="id"></param>
            /// <param name="mp"></param>
            /// <param name="admin"></param>
        public Compte(string id, string mp, bool admin)
        {
            Identifiant = id;
            motDePasse = mp;
            EstAdmin = admin;
            LesLangagesFavoris = new List<Langage>();
            HistoriqueDuCompte = new List<Langage>();
        }

            /// <summary>
            /// ce second constructeur prend en parametre seulement un identifiant et un mot de passe de type string
            /// ce constructeur sert à creer un compte sans specifier l'acces au mode amdinistrateur ; cette propriété sera 
            /// alors set à false (ce constructeur a surtout été utile pour les tests)
            /// </summary>
            /// <param name="id"></param>
            /// <param name="mp"></param>
        public Compte(string id, string mp): this(id, mp, false){}

        /// <summary>
        /// la methode ToString permet d'afficher les différentes informations d'un compte
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            string admin;
            if (EstAdmin == false)
            {
                admin = "ce compte n'a pas l'acces au mode administrateur";
            }
            else 
            {
                admin = "ce compte a l'acces au mode administrateur";
            }

            return $"Identifiant = {Identifiant} \n Mot de passe = {MotDePasse} \n Mode Administrateur = {admin}";
        }

        /// <summary>
        /// ces trois methodes servent a redéfinir protocole d'égalité
        /// deux comptes sont les mêmes si ils ont le même identifiant et mot de passe
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {
            if (obj == this) return true;
            if (obj == null) return false;
            if (obj.GetType() != this.GetType()) return false;
            return Equals(obj as Compte);
        }

        public bool Equals(Compte other)
        {
            return (other.Identifiant == this.Identifiant) && (other.MotDePasse == this.MotDePasse);
        }

        public override int GetHashCode()
        {
            return ((Identifiant.Length + MotDePasse.Length) * HistoriqueDuCompte.Count) / 3 ;
        }

        /// <summary>
        /// cette méthode sert à ajouter un langage à une liste de langage
        /// elle va être utilisé par les methodes AjouterUnLangageALHistorique et AjouterUnLangageAuxFavoirs
        /// elle prend alors en parametre la liste de Langage correspondante et le Langage à ajouter
        /// </summary>
        /// <param name="tmp"></param>
        /// <param name="l"></param>
        private IList<Langage> AjouterUnLangageAUneListeDeLangages(IList<Langage> tmp ,Langage l)
        {
            try
            {
                tmp.Add(l);
            }
            catch (NotSupportedException)
            {
                tmp = new List<Langage>(tmp);
                tmp.Add(l);
            }
            return tmp;
        }

        private IList<Langage> RetirerUnLangageAUneListeDeLangage(IList<Langage> tmp, Langage l)
        {
            try
            {
                tmp.Remove(l);
            }
            catch(NotSupportedException)
            {
                tmp = new List<Langage>(tmp);
                tmp.Remove(l);
            }
            return tmp;
        }

        /// <summary>
        /// cette méthode sert à ajouter un langage à l'historique du compte
        /// elle prend un langage en parametre, et appelle la methode AjouterUnLangageAUneListeDeLangages en lui donnant
        /// la liste HistoriqueDuCompte ainsi que le langage à ajouter à la liste
        /// </summary>
        /// <param name="l"></param>
        public void AjouterUnLangageALHistorique(Langage l)
        {
            HistoriqueDuCompte = AjouterUnLangageAUneListeDeLangages(HistoriqueDuCompte, l);
        }

        /// <summary>
        /// cette méthode sert à ajouter un langage aux favoris du compte
        /// elle prend un langage en parametre, et appelle la methode AjouterUnLangageAUneListeDeLangages en lui donnant
        /// la liste LesLangagesFavoris ainsi que le langage à ajouter à la liste
        /// </summary>
        /// <param name="l"></param>
        public void AjouterUnLangageAuxFavoirs(Langage l)
        {
            LesLangagesFavoris = AjouterUnLangageAUneListeDeLangages(LesLangagesFavoris, l);
        }

        public void RetirerUnLangageDesFavoris(Langage l)
        {
            LesLangagesFavoris = RetirerUnLangageAUneListeDeLangage(LesLangagesFavoris, l);
        }
    }
}
