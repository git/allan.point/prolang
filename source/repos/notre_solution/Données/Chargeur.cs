﻿using notre_bibliotheque;
using System;
using System.Collections.Generic;
using System.Text;

namespace données
{
    public abstract class Chargeur : IChargeurDeCompte, IChargeurDeLangage, IChargeurDItems, IChargeurDeGestionaire

    {
        public Chargeur(string path)
        {

        }

        public abstract Gestionaire ChargerGestionaireDeCompte();
        public abstract Gestionaire ChargerGestionaireDeLangage();
        public abstract IList<Item> ChargerLesComptes();
        public abstract IList<Items> ChargerLesItems();
        public abstract IList<Item> ChargerLesLangages();
    }
}
