﻿using notre_bibliotheque;
using System;
using System.Collections.Generic;
using System.Text;

namespace données
{
    /// <summary>
    /// cette classe sert à instancier tous les langages et les comptes
    /// ce sont les données qui seront utilisées dans l'application
    /// </summary>
    public class Data
    {
        //toutes les listes d'auteurs
        public static IList<string> LesAuteurs { get; set; } = new List<string>() { "inconnue", };
        public static IList<string> KotlinAuteurs { get; set; } = new List<string>() { "JetBrains", };
        public static IList<string> JavaAuteurs { get; set; } = new List<string>() { "Sun Microsystems", };
        public static IList<string> DartAuteurs { get; set; } = new List<string>() { "Lars Bak", "Kasper Lund", };
        public static IList<string> CAuteurs { get; set; } = new List<string>() { "Dennis Ritchie", "Brian Kernighan", };
        public static IList<string> BashAuteurs { get; set; } = new List<string>() { "Brian Fox", };
        public static IList<string> GoAuteurs { get; set; } = new List<string>() { "Robert Griesemer", "Rob Pike", "Ken Thompson", };
        public static IList<string> FsharpAuteurs { get; set; } = new List<string>() { "Don Syme", "Microsoft Research", };
        public static IList<string> PythonAuteurs { get; set; } = new List<string>() { "Guido Van Rossum", };
        public static IList<string> CPlusPlusAuteurs { get; set; } = new List<string>() { "Bjarne Stroustrup", };
        public static IList<string> JavaScriptAuteurs { get; set; } = new List<string>() { "Brenden Eich", };
        public static IList<string> CsharpAuteurs { get; set; } = new List<string>() { "Microsoft", };
        public static IList<string> RubyAuteurs { get; set; } = new List<string>() { "Yukihiro Matsumoto", };
        public static IList<string> PhpAuteurs { get; set; } = new List<string>() { "Ramus Lerdorf", };

        //toutes les listes de logiciels
        public static IList<string> LesLogiciels { get; set; } = new List<string>() { "inconnue",};
        public static IList<string> JavaLogiciels { get; set; } = new List<string>() { "Eclipse", "Azureus","OpenOffice (couplé à du C++)","Tout l'environnement Android et BlackBerry",};
        public static IList<string> GoLogiciels { get; set; } = new List<string>() { "Google Map", "Google Music", "Youtube", };
        public static IList<string> PythonLogiciels { get; set; } = new List<string>() { "Instagram", "Spotify", "Raspberry Pi", };
        public static IList<string> CPlusPlusLogiciels { get; set; } = new List<string>() { "Interface Utilisateur IPod", "Microsoft Windows OS", "OpenOffice (couplé à du Java)", "Microsoft Office", "Dobe Photoshop", "Mozilla Firefox", };
        public static IList<string> RubyLogiciels { get; set; } = new List<string>() { "Rubygems", };



        //toutes les listes de paradigmes
        public static IList<string> LesParadigmes { get; set; } = new List<string>(){"Imperatif" ,"Procédural","Objet","Fonctionel",};
        public static IList<string> KotlinParadigmes { get; set; } = new List<string>() { "Objet", "Fonctionel", };
        public static IList<string> DartParadigmes { get; set; } = new List<string>() { "Objet", };
        public static IList<string> CParadigmes { get; set; } = new List<string>() { "Impératif", "Procédural", "Structuré", };
        public static IList<string> JavaParadigmes { get; set; } = new List<string>() { "Objet", "Structuré", "Impératif", "Fonctionel", };
        public static IList<string> GoParadigmes { get; set; } = new List<string>() { "Langage Compilé", "Programmation Concurrente", "Imprératif", "Structuré", };
        public static IList<string> FsharpParadigmes { get; set; } = new List<string>() { "Fonctionnel", "Impératif", "Objet", };
        public static IList<string> PythonParadigmes { get; set; } = new List<string>() { "Fonctionnel", "Impératif", "Objet", };
        public static IList<string> CPlusPlusParadigmes { get; set; } = new List<string>() { "Générique", "Objet", "Procédural", };
        public static IList<string> JavaScriptParadigmes { get; set; } = new List<string>() { "Script", "Objet", "Impératif", "Fonctionnel", "Orienté Prototype", };
        public static IList<string> CsharpParadigmes { get; set; } = new List<string>() { "Structuré", "Impératif", "Objet", };
        public static IList<string> RubyParadigmes { get; set; } = new List<string>() { "Interpreté", "Objet", "Impératif", "Concurrent", "Fonctionnel", };
        public static IList<string> PhpParadigmes { get; set; } = new List<string>() { "Procédural", "Objet", "Impératif", "Reflexif", "Fonctionnel", "Interpreté", };

        //tous les exemples
        public static string javaExemple = "class HelloWorld {\npublic static void main(String[] args){\nSystem.out.println(\"Hello, World!\");\n}\n}";
        public static string dartExemple = "void main() {\nprint('Hello World!');\n}";
        public static string bashExemple = "#!/bin/bash\nvar=\"Hello World\"\necho \"$var\"";
        public static string cExemple = "#include <stdio.h>\nint main(void){\nprintf(\"hello, world\\n\");\nreturn 0;\n}";
        public static string kotlinExemple = "class Greeter(val name: String) {\nfun greet() {\nprintln(\"Hello, $name\")\n}\n}\n\nfun main(args: Array<String>) {\nGreeter(args[0]).greet()\n}";
        public static string goExemple = "package main\nimport \"fmt\"\nfunc main(){\nfmt.Printf(\"Hello, world\\n\")\n}";
        public static string cPlusPlusExemple = "#include<iostream>\nint main()\n{\nstd::cout << \"Hello, new world!\\n\";\n}";
        public static string rubyExemple = "# The Greeter class\nclass Greeter\ndef initialize(name)\n@name = name.capitalize\nend\n\ndef salute\nputs \"Hello #{@name}!\"\nend\nend\n# Create a new object\ng = Greeter.new(\"world\")\n# Output \"Hello World!\"\ng.salute";
        public static string phpExemple = "<?php\necho 'Hello World';\n?>\n\n ou \n\n<?= 'Hello World' ?>";
        public static string fsharpExemple = "printfn \"Hello World!\"";
        public static string pythonExemple = "# src/hello.py\nprint(\"Hello world!\")";
        public static string javaScriptExemple = "console.log('Hello world');";
        public static string csharpExemple = "Console.WriteLine(\"Hello World!\");";

        //les instances des langages dans la liste LesLangages
        public static IList<Item> LesLangages { get; set; } = new List<Item>() {
                new Langage("Kotlin", 2010, KotlinAuteurs, "https://kotlinlang.org/docs/home.html", "kotlin.png", kotlinExemple, LesLogiciels, "permet de coder lol", KotlinParadigmes, 4),
                new Langage("Dart", 2011, DartAuteurs, "https://dart.dev/", "dart.png", dartExemple, LesLogiciels, "Ce langage est utilisé pour créer des applications mobiles,\nde bureau, de serveur et web.", DartParadigmes, 1),
                new Langage("C", 1972,  CAuteurs, "https://devdocs.io/c/", "c.png", cExemple, LesLogiciels, "Ce langage est utilisé pour les systèmes d’exploitation,\nle développement de logiciel, et le hardware.", CParadigmes, 1),
                new Langage("Bash", 1989, BashAuteurs, "https://www.gnu.org/software/bash/manual/", "bash.png", bashExemple, LesLogiciels, "Ce langage permet d'écrire des scripts", LesParadigmes, 5),
                new Langage("Go", 2009, GoAuteurs, "https://golang.org/", "go.png", goExemple, GoLogiciels, "Ce langage est utiliser aussi bien pour écrire des applications,\ndes scripts ou de grands systèmes.", GoParadigmes, 1),
                new Langage("F#", 2002, FsharpAuteurs, "https://fsharp.org/", "fsharp.png", fsharpExemple, LesLogiciels, "Ce langage vous permet de développer ce que vous voulez.", FsharpParadigmes, 1),
                new Langage("Java",1995, JavaAuteurs, "https://docs.oracle.com/en/java/javase/11/docs/api/index.html", "java.svg.png",javaExemple,JavaLogiciels,"Ce langage permet de créer et faire fonctionner\ndes applications pour ordinateurs,  également utilisé sur la plupart des autres\nsupports numériques (smartphones, consoles, logiciels).",JavaParadigmes,1),
                new Langage("Python", 1991,PythonAuteurs,"https://www.python.org/doc/","python.png",pythonExemple,PythonLogiciels,"Ce langage est utilisé pour le developpement web,\nde jeux videos, de logiciels, des interfaces utilisateurs\ngraphiques, ainsi en tant que langage de script.",PythonParadigmes,1),
                new Langage("C++", 1983,CPlusPlusAuteurs,"https://isocpp.org/","c++.png",cPlusPlusExemple,CPlusPlusLogiciels,"Ce langage est utilisé pour développer des logiciels,\ndes systèmes d’exploitation, des jeux vidéos et des moteurs de recherche.",CPlusPlusParadigmes,1),
                new Langage("JavaScript", 1996,JavaScriptAuteurs,"https://developer.mozilla.org/fr/docs/Web/JavaScript","javascript.svg.png",javaScriptExemple,LesLogiciels,"Ce langage est souvent utilisé dans les devantures de sites,\nles outils analytiques, les widgets et les interactions web",JavaScriptParadigmes,1),
                new Langage("C#",2001,CsharpAuteurs,"https://isocpp.org/","csharp.png",csharpExemple,LesLogiciels,"Ce langage vous permet de développer ce que vous voulez.",CsharpParadigmes,1),
                new Langage("Ruby",1995,RubyAuteurs,"https://www.ruby-lang.org/en/","ruby.svg.png",rubyExemple,RubyLogiciels,"Ce langage est utilisé pour les interfaces utilisateurs graphiques,\nles applications web et le développement web.",RubyParadigmes,1),
                new Langage("PHP",1994,PhpAuteurs,"https://www.php.net/","php.svg.png",phpExemple,LesLogiciels,"Ce langage est utilisé pour le développement web, mais également\ncomme langage de programmation d’usage général.",PhpParadigmes,1),
            };

        //les instances des comptes dans la liste LesComptes
        public static IList<Item> LesComptes { get; set; } = new List<Item>() {
                new Compte("id1", "mdp1",true),
                new Compte("id2", "mdp1"),
                new Compte("id3", "mdp1"),
                new Compte("id4", "mdp1"),
                new Compte("id5", "mdp1"),
                new Compte("id6", "mdp1"),
                new Compte("id7", "mdp1"),
                new Compte("id8", "mdp1"),
                new Compte("id9", "mdp1"),
                new Compte("id10", "mdp1"),
                new Compte("id12", "mdp1"),
            };
    }
}
