﻿using notre_bibliotheque;
using System;
using System.Collections.Generic;


namespace données
{
    /// <summary>
    /// cette classe sert à charger et sauvegarder les instances de toutes les données (langages et comptes)
    /// elle implemente l'interface IPercistance
    /// </summary>
    public class Stub : IPercistance
    {
        //le constructeur de la classe prend un string en parametre
        public Stub(string path) {}

        /// <summary>
        /// cette methode sert à charger les données; 
        /// elle crée un dictionnaire avec une clé de type string et une valeur de type IEnumerable<Item>,
        /// elle ajoute deux éléments dans le dictionnaire : une clé Langages avec en valeur la liste LesLangages de Data, 
        /// et une clé Comptes avec en valeur la liste LesComptes de Data
        /// </summary>
        /// <returns> elle retourne le dictionnaire </returns>
        public Dictionary<string, IEnumerable<Item>> ChargerLesDonnées()
        {
            Dictionary<string, IEnumerable<Item>> tmp = new Dictionary<string, IEnumerable<Item>>();
            tmp.Add("Langages", Data.LesLangages);
            tmp.Add("Comptes", Data.LesComptes);
            return tmp;
        }

        /// <summary>
        /// cette méthode sert à sauvegarder les données;
        /// elle prend deux parametres, une collection IEnumérable<Langage> et une collection IEnumerable<Compte>
        /// ces collections sont les listes de langages et comptes à sauvegarder
        /// </summary>
        /// <param name="langagesÀSauvegarder"></param>
        /// <param name="comptesÀSauvegarder"></param>
        public void SauvegarderLesDonnées(IEnumerable<Langage> langagesÀSauvegarder , IEnumerable<Compte> comptesÀSauvegarder)
        {
            return;
        }
    }
}
