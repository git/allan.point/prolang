﻿using notre_bibliotheque;
using System;
using System.Collections.Generic;
using System.Text;

namespace données
{
    interface IChargeurDItems
    {
        IList<Items> ChargerLesItems();
    }
}
