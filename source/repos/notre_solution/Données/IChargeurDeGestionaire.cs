﻿using notre_bibliotheque;
using System;
using System.Collections.Generic;
using System.Text;

namespace données
{
    public interface IChargeurDeGestionaire
    {
        Gestionaire ChargerGestionaireDeLangage();
        Gestionaire ChargerGestionaireDeCompte();
    }
}
