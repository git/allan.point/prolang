﻿using données;
using notre_bibliotheque;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace test_number_one
{
    class Program
    {
        /// <summary>
        /// cette classe sert de test fonctionnel; toutes les actions possibles de l'application sont testées
        /// </summary>
        
        //cette attribut de type IPercistance va permettre de charger les données depuis Stub
        private static IPercistance stb = new Stub("");

        static void Main(string[] args)
        {
            TestGlobal();
        }

        static void TestGlobal()
        {
            //cette collection de type IList<Langage> recupère les langages chargés
            IList<Item> lesLangages = stb.ChargerLesDonnées()["Langages"] as IList<Item>;
            Items itemsLang = new Items(lesLangages);

            //cette collection de type IList<Compte> recupère les comptes chargés
            IList<Item> lesComptes = stb.ChargerLesDonnées()["Comptes"] as IList<Item>;
            Item it11 = new Compte("id13", "mdp1");
            lesComptes.Add(it11);

            //ces attributs de type Gestionaire vont permettre la gestion des données à travers les tests
            Gestionaire.Persistance = stb;
            Gestionaire gestionaireDeComptes = new GestionaireDeComptes();
            Gestionaire gestionaireDeLanagage = new GestionaireDeLangages();


            bool l = true;

            //cette méthode permet d'afficher tous les langages, pour vérifier que toutes les informations sont correctes
            //elle demande à l'utilisateur de préciser le type de tri, puis affiche la liste selon
            void AfficherLesLangages(GestionaireDeLangages gl, GestionaireDeComptes gc)
            {
                Console.WriteLine("Comment Afficher les langages");
                Console.WriteLine("1. Trier par nom");
                Console.WriteLine("2. Afficher selement les favoris");
                Console.WriteLine("3. Trier par ordre de date de création");
                Console.WriteLine("4. Trier par ordre de génération");


                string rép = Console.ReadLine();
                switch (rép)
                {
                    case "2":
                        if(gc.ItemsComptes.ItemCourant == null)
                        {
                            Console.WriteLine("Persone n'est connecté");
                            return;
                        }
                        if((gc.ItemsComptes.ItemCourant as Compte).LesLangagesFavoris.Count == 0)
                        {
                            Console.WriteLine($"{(gc.ItemsComptes.ItemCourant as Compte).Identifiant} n'a pas de langage favoris");
                            return;
                        }
                        gl.ItemsLangages.Filtre = ValeurTri.Favoris;
                        break;
                    case "3":
                        gl.ItemsLangages.Filtre = ValeurTri.Date;
                        break;
                    case "4":
                        gl.ItemsLangages.Filtre = ValeurTri.Génération;
                        break;
                    case "1":
                    default:
                        gl.ItemsLangages.Filtre = ValeurTri.Nom;
                        break;
                }
                foreach (Item it in gl.ItemsLangages.LesItems)
                {
                    Console.WriteLine($"{it}\n**************************************************************************");
                }
            }

            //cette méthode permet d'afficher tous les comptes, pour vérifier que toutes les informations sont correctes
            void AfficherLesComptes(GestionaireDeComptes g)
            {
                foreach (Item it in g.ItemsComptes.LesItems)
                {
                    Console.WriteLine($"{it}\n**************************************************************************");
                }
            }

            //cette méthode permet de créer un compte ; la console demande à l'utilisateur de saisir 
            //toutes les informations nécessaires
            //si il n'y a pas d'erreurs (les deux mots de passes sont identiques et l'identifiant n'est pas déjà utilisé
            //par un autre utilisateur), le message "Votre compte à bien été créé" s'affiche et l'utilisateur est 
            //automatiquement conecté à ce compte
            void CreerUnCompte(GestionaireDeComptes g)
            {
                Console.Write("ID :");
                string id = Console.ReadLine();
                Console.Write("Mot de passe :");
                string mdp1 = Console.ReadLine();
                Console.Write("Mot de passe :");
                string mdp2 = Console.ReadLine();
                Console.Write("Accès au mode administrateur ? (o/n) : ");
                string rep = Console.ReadLine();
                bool admin;
                if (rep == "o")
                {
                    admin = true;
                }
                else admin = false;
                try
                {
                    Compte tmp = g.VerfierCreationCompte(id, mdp1, mdp2,admin);
                    g.ItemsComptes.Ajouter(tmp);
                    g.ItemsComptes.ItemCourant = tmp;
                    Console.WriteLine("Votre compte à bien été créé");
                }catch(Exception e)
                {
                    Console.WriteLine(e.Message);
                }

            }

            //cette méthode sert à supprimer un compte ; la console demande à l'utilisateur de saisir les informations
            //nécessaires avant de supprimer le compte
            //si il n'y a pas d'erreurs (l'identifiant et mot de passe sont correctes et existent bien 
            //dans le liste de comptes), le message "-identifiant- à bien été supprimé" s'affiche
            void SupprimerUnCompte(GestionaireDeComptes g)
            {
                Console.Write("ID :");
                string id = Console.ReadLine();
                Console.Write("MDP :");
                string mdp = Console.ReadLine();
                Compte tmp = new Compte(id, mdp);
                try
                {
                    g.ItemsComptes.Supprimer(tmp);
                    Console.WriteLine($"{tmp.Identifiant} à bien était supprimé");
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }

            }

            //cette méthode sert à ajouter un langage ; pour se faire, l'utilisateur doit être connecté, et doit avoir
            //le droit d'accès au mode administrateur (sinon, un message d'erreur s'affiche)
            //la console demande à l'utilisateur de saisir toutes les informations nécessaires 
            //pour remplir un champ, tant qu'il y a une erreur (qu'une exception est lévée), l'utilisateur devra 
            //ressaisir l'information jusqu'a ce qu'elle soit correcte pour continuer
            //si il n'y a pas d'erreur, le message "Le Langage a bien été ajouté à la liste" est affiché, 
            //et le langage est ajouté
            void AjouterUnLangage(GestionaireDeLangages gl, GestionaireDeComptes gc)
            {
                if (gc.IsSomeoneConnected)
                {
                    if ((gc.ItemsComptes.ItemCourant as Compte).EstAdmin)
                    {
                        bool success = false;
                        string respuesta;
                        string nom = "";
                        string aut = "";
                        string doc = "";
                        string logo = "";
                        string log = "";
                        string utilite = "";
                        int gen = 0;
                        int date = 0;
                        while (!success) 
                        {
                            Console.Write("Nom : ");
                            nom = Console.ReadLine();
                            if(string.IsNullOrEmpty(nom.Trim()))
                            {
                                Console.WriteLine("Le nom doit être saisi");
                                continue;
                            }
                            success = true;
                        }
                        IList<string> auteurs = new List<string>();
                        success = false;
                        while (!success)
                        {
                            do
                            {
                                Console.Write("Un Auteur : ");
                                aut = Console.ReadLine();
                                auteurs.Add(aut);
                                Console.Write("D'auteurs auteurs ? (o/n) : ");
                                respuesta = Console.ReadLine();
                            } while (respuesta != "n");
                            if(auteurs.Count == 0)
                            {
                                Console.WriteLine("Il doit y avoir au moins un auteur");
                                continue;
                            }
                            success = true;
                        }

                        success = false;
                        while (!success)
                        {
                            Console.Write("Date de création (aaaa) : ");
                            try
                            {
                                date = int.Parse(Console.ReadLine());
                                success = true;
                            }
                            catch (Exception e)
                            {
                                Console.WriteLine(e.Message);
                            }
                        }

                        success = false;
                        while (!success)
                        {
                            Console.Write("Lien vers la documentation : ");
                            doc = Console.ReadLine();
                            if(String.IsNullOrEmpty(doc.Trim()))
                            {
                                Console.WriteLine("Le lien vers la documentation doit être rempli");
                                continue;
                            }
                            success = true;
                        }

                        success = false;
                        while (!success)
                        {
                            Console.Write("Logo (chemin) : ");

                            logo = Console.ReadLine();
                            if(string.IsNullOrEmpty(logo.Trim()))
                            {
                                Console.WriteLine("Le chemin doit être saisi");
                                continue;
                            }
                            success = true;
                        }

                        IList<string> logiciels = new List<string>();
                        success = false;
                        while (!success)
                        {
                            do
                            {
                                Console.Write("Un logiciel : ");
                                log = Console.ReadLine();
                                logiciels.Add(log);
                                Console.Write("D'auteurs logiciels ? (o/n) : ");
                                respuesta = Console.ReadLine();
                            } while (respuesta != "n");
                            if(logiciels.Count == 0)
                            {
                                Console.WriteLine("Il faut saisir des logiciels");
                                continue;
                            }
                            success = true;
                        }

                        bool finish = false;
                        string exemple = "";
                        success = false;
                        while (!success)
                        {
                            Console.WriteLine("Exemple de code (Hello World) - tapez entrer pour ecrire une nouvelle ligne");
                            while (!finish)
                            {
                                exemple += $"{Console.ReadLine()}\n";
                                Console.Write("Ecrire une autre ligne ? (o/n) : ");
                                respuesta = Console.ReadLine();
                                if (respuesta == "n")
                                {
                                    finish = true;
                                }
                            }
                            if(String.IsNullOrEmpty(exemple.Trim()))
                            {
                                Console.WriteLine("L'exemple doit être saisi");
                                continue;
                            }
                            success = true;
                        }

                        success = false;
                        while (!success)
                        {
                            Console.Write("Utilités du langage : ");

                            utilite = Console.ReadLine();
                            if(string.IsNullOrEmpty(utilite))
                            {
                                Console.WriteLine("Il faut saisir l'utilité du langage");
                                continue;
                            }
                            success = true;

                        }

                        IList<string> paradigmes = new List<string>();
                        string parag;
                        success = false;
                        while (!success)
                        {
                            do
                            {
                                Console.Write("Un paradigme : ");
                                parag = Console.ReadLine();
                                paradigmes.Add(parag);
                                Console.Write("D'auteurs paradigmes ? (o/n) : ");
                                respuesta = Console.ReadLine();
                            } while (respuesta != "n");

                            if(paradigmes.Count == 0)
                            {
                                Console.WriteLine("Il doit y avoir au moins un paradigme");
                                continue;
                            }
                            success = true;
                        }
                        success = false;
                        while(!success)
                        {
                            Console.Write("Définir une génération :");
                            try
                            {
                                gen = int.Parse(Console.ReadLine());
                                if(gen <= 0)
                                {
                                    Console.WriteLine("La génération du langage doit être superieur à 0");
                                    continue;
                                }
                            }catch(System.IO.IOException e)
                            {
                                Console.WriteLine(e.Message);
                            }
                            success = true;
                        }
                        Langage lang = new Langage(nom, date, auteurs, doc, logo, exemple, logiciels, utilite, paradigmes, gen);
                        gl.ItemsLangages.Ajouter(lang);
                        if (gl.ItemsLangages.Exists(lang))
                        {
                            Console.WriteLine("Le Langage a bien été ajouté à la liste");
                            Console.WriteLine("*****************************************");
                            Console.WriteLine(lang);
                        }
                        else
                        {
                            Console.WriteLine("pb : le langage n'a pas été ajouté");
                        }
                    }
                    else
                    {
                        Console.WriteLine("Vous n'avez pas accès au mode adminstrateur");
                    }
                }
                else
                {
                    Console.WriteLine("Vous n'êtes pas connecté");
                }
            }

            //cette méthode sert à modifier un langage; pour se faire, l'utilisateur doit être connecté, doit avoir
            //le droit d'accès au mode administrateur, et avoir préalablement sélectionné un langage
            //(sinon, un message d'erreur s'affiche)
            //la console demande à l'utilisateur de saisir toutes les informations nécessaires 
            //pour remplir un champ, tant qu'il y a une erreur (qu'une exception est lévée), l'utilisateur devra 
            //ressaisir l'information jusqu'a ce qu'elle soit correcte pour continuer
            //pour chaque champ que l'utilisateur choisi de modifier, la propriété du langage est set par la valeur saisie
            //si il n'y a pas d'erreur, le message "Le Langage a bien été modifiée" est affiché
            void ModifierLangageSelectionné(GestionaireDeLangages gl, GestionaireDeComptes gc)
            {
                if (gc.IsSomeoneConnected)
                {
                    if ((gc.ItemsComptes.ItemCourant as Compte).EstAdmin)
                    {
                        if (gl.IsASelectedLanguage)
                        {
                            bool success = false;
                            Console.WriteLine(gl.ItemsLangages.ItemCourant);
                            string answer;
                            Console.Write("Modifier le nom ? (o/n) : ");
                            answer = Console.ReadLine();
                            if(answer == "o")
                            {
                                while (!success)
                                {
                                    Console.Write("Nouveau nom : ");
                                    try
                                    {
                                        (gl.ItemsLangages.ItemCourant as Langage).Nom = Console.ReadLine();
                                        success = true;
                                    }
                                    catch (Exception e)
                                    {
                                        Console.WriteLine(e.Message);
                                    }
                                }
                                success = false;
                            }
                            Console.Write("Modifier la date de création ? (o/n) : ");
                            answer = Console.ReadLine();
                            if(answer == "o")
                            {
                                while (!success)
                                {
                                    Console.Write("aaaa : ");
                                    try
                                    {
                                        (gl.ItemsLangages.ItemCourant as Langage).DateDuLangage = int.Parse(Console.ReadLine());
                                        success = true;
                                    }
                                    catch (Exception e)
                                    {
                                        Console.WriteLine(e.Message);
                                    }
                                }
                                success = false;
                            }
                            Console.Write("Modifier les auteurs ? (o/n) : ");
                            answer = Console.ReadLine();
                            if(answer == "o")
                            {
                                IList<string> auteurs = new List<string>();
                                string aut;
                                string reponse;
                                while (!success)
                                {
                                    do
                                    {
                                        Console.Write("Un Auteur : ");
                                        aut = Console.ReadLine();
                                        auteurs.Add(aut);
                                        Console.Write("D'auteurs auteurs ? (o/n) : ");
                                        reponse = Console.ReadLine();
                                    } while (reponse != "n");
                                    try
                                    {
                                        (gl.ItemsLangages.ItemCourant as Langage).LesAuteurs = auteurs;
                                        success = true;
                                    }
                                    catch(Exception e)
                                    {
                                        Console.WriteLine(e.Message);
                                    }
                                }
                                success = false;
                            }
                            Console.Write("Modifier le logo ? (o/n) : ");
                            answer = Console.ReadLine();
                            if(answer == "o")
                            {
                                while (!success)
                                {
                                    Console.Write("Nouveau chemin du logo : ");
                                    try
                                    {
                                        (gl.ItemsLangages.ItemCourant as Langage).CheminDuLogo = Console.ReadLine();
                                        success = true;
                                    }
                                    catch (Exception e)
                                    {
                                        Console.WriteLine(e.Message);
                                    }
                                }
                                success = false;
                            }
                            Console.Write("Modifier la documentation ? (o/n) : ");
                            answer = Console.ReadLine();
                            if(answer == "o")
                            {
                                while (!success)
                                {
                                    Console.Write("Nouveau lien vers la documentation : ");
                                    try
                                    {
                                        (gl.ItemsLangages.ItemCourant as Langage).Documentation = Console.ReadLine();
                                        success = true;
                                    }
                                    catch(Exception e)
                                    {
                                        Console.WriteLine(e.Message);
                                    }
                                }
                                success = false;
                            }
                            Console.Write("Modifier les logiciels ? (o/n) : ");
                            answer = Console.ReadLine();
                            if(answer == "o")
                            {
                                IList<string> logiciels = new List<string>();
                                string log;
                                string reponse;
                                while (!success)
                                {
                                    do
                                    {
                                        Console.Write("Un logiciel : ");
                                        log = Console.ReadLine();
                                       logiciels.Add(log);
                                        Console.Write("D'auteurs logiciels ? (o/n) : ");
                                        reponse = Console.ReadLine();
                                    } while (reponse != "n");
                                    try
                                    {
                                        (gl.ItemsLangages.ItemCourant as Langage).LogicielsConus = logiciels;
                                        success = true;
                                    }
                                    catch(Exception e)
                                    {
                                        Console.WriteLine(e.Message);
                                    }
                                }
                                success = false;
                            }
                            Console.Write("Modifier l'utilité ? (o/n) : ");
                            answer = Console.ReadLine();
                            if(answer == "o")
                            {
                                while (!success)
                                {
                                    Console.Write("Nouvelle utilité : ");
                                    try
                                    {
                                        (gl.ItemsLangages.ItemCourant as Langage).UtilitéDuLangage = Console.ReadLine();
                                        success = true;
                                    }
                                    catch (Exception e)
                                    {
                                        Console.WriteLine(e.Message);
                                    }
                                }
                                success = false;
                            }
                            Console.Write("Modifier l'exemple ? (o/n) : ");
                            answer = Console.ReadLine();
                            if (answer == "o")
                            {
                                bool finish = false;
                                string exemple = "";
                                while (!success)
                                {
                                    while (!finish)
                                    {
                                        exemple += $"{Console.ReadLine()}\n";
                                        Console.Write("Ecrire une autre ligne ? (o/n) : ");
                                        answer = Console.ReadLine();
                                        if (answer == "n")
                                        {
                                            finish = true;
                                        }
                                    }
                                    try
                                    {
                                        (gl.ItemsLangages.ItemCourant as Langage).ExempleDeCode = exemple;
                                        success = true;
                                    }
                                    catch (Exception e)
                                    {
                                        Console.WriteLine(e.Message);
                                    }
                                }
                                success = false;
                            }
                            Console.Write("Modifier les paradigmes ? (o/n) : ");
                            answer = Console.ReadLine();
                            if(answer == "o")
                            {
                                IList<string> paradigmes = new List<string>();
                                string parag;
                                string reponse;
                                while (!success)
                                {
                                    do
                                    {
                                        Console.Write("Un paradigme : ");
                                        parag = Console.ReadLine();
                                        paradigmes.Add(parag);
                                        Console.Write("D'auteurs paradigmes ? (o/n) : ");
                                        reponse = Console.ReadLine();
                                    } while (reponse != "n");
                                    try
                                    {
                                        (gl.ItemsLangages.ItemCourant as Langage).LesParadigmes = paradigmes;
                                        success = true;
                                    }
                                    catch (Exception e)
                                    {
                                        Console.WriteLine(e.Message);
                                    }
                                }
                                success = false;
                            }
                        }
                        else
                        {
                            Console.WriteLine("Vous n'avez séléctioné aucun langage");
                        }
                    }
                    else
                    {
                        Console.WriteLine("Vous n'avez pas accès au mode adminstrateur");
                    }
                }
                else
                {
                    Console.WriteLine("Vous n'êtes pas connécté");
                }
            }

            //cette méthode sert à supprimer un langage; pour se faire, l'utilisateur doit être connecté, doit avoir
            //le droit d'accès au mode administrateur, et avoir préalablement sélectionné un langage
            //(sinon, un message d'erreur s'affiche)
            //la console demande à l'utilisateur de confirmer la suppression du langage selectionné
            //si l'uilsateur choisi de supprimer la langage et s'il n'y a pas d'erreur, 
            //le message "Le Langage a bien été supprimé" est affiché, le langage est ainsi supprimé, et le langage selectionné 
            //devient null
            void SupprimerLangageSelectionné(GestionaireDeLangages gl, GestionaireDeComptes gc )
            {
                if (gc.IsSomeoneConnected)
                {
                    if ((gc.ItemsComptes.ItemCourant as Compte).EstAdmin)
                    {
                        if (gl.IsASelectedLanguage)
                        {
                            try
                            {
                                Console.WriteLine(gl.ItemsLangages.ItemCourant);
                                Console.Write("Supprimer ce langage ? (o/n) : ");
                                string answer = Console.ReadLine();
                                if (answer == "o")
                                {
                                    gl.ItemsLangages.Supprimer((gl.ItemsLangages.ItemCourant as Langage)); 
                                    if (gl.ItemsLangages.Exists((gl.ItemsLangages.ItemCourant as Langage)))
                                    {
                                        Console.WriteLine("pb : le langage n'a pas été ajouté");
                                    }
                                    else
                                    {
                                        Console.WriteLine($"Le langage à bien était supprimé");
                                        gl.ItemsLangages.ItemCourant = null;
                                    }
                                }
                            }
                            catch (Exception e)
                            {
                                Console.WriteLine(e.Message);
                            }
                        }
                        else
                        {
                            Console.WriteLine("Vous n'avez séléctioné aucun langage");
                        }
                    }
                    else
                    {
                        Console.WriteLine("Vous n'avez pas accès au mode administrateur");
                    }
                }
                else
                {
                    Console.WriteLine("Vous n'êtes pas connécté");
                }

            }

            //cette méthode sert à modifier le mot de passe d'un compte ; pour se faire, l'utilisater doit être connecté
            //(sinon un message d'erreur est affiché)
            //tant que les deux mots de passes saisis ne sont pas les même, l'utilisateur devra ressaisir 
            //les mots de passes pour continuer
            //si il n'y a pas d'erreur, le message "Mot de passe modifié avec succès" est affiché, 
            //et le mot de passe est set
            void ModifierCompte(GestionaireDeComptes g) 
            {
                if (g.IsSomeoneConnected)
                {
                    Console.WriteLine("Le compte courant : ");
                    Console.WriteLine(g.ItemsComptes.ItemCourant);
                    Console.WriteLine("**********************************");
                    Console.Write("Nouveau mot de passe : ");
                    string mdp = Console.ReadLine();
                    Console.Write("Confirmer le mot de passe : ");
                    string mdp2 = Console.ReadLine();
                    while (mdp != mdp2)
                    {
                        Console.WriteLine("Les deux mots de passe ne sont pas les mêmes; veuillez réessayer");
                        Console.Write("Nouveau mot de passe : ");
                        mdp = Console.ReadLine();
                        Console.Write("Confirmer le mot de passe : ");
                        mdp2 = Console.ReadLine();
                    }
                   (g.ItemsComptes.ItemCourant as Compte).MotDePasse = mdp;
                    Console.WriteLine("Le compte courant : ");
                    Console.WriteLine(g.ItemsComptes.ItemCourant);
                    Console.WriteLine("**********************************");
                    Console.WriteLine("Mot de passe modifié avec succès");
                }
                else
                {
                    Console.WriteLine("Vous n'êtes pas connecté");
                }
                
            }

            //cette méthode permet à l'utilisateur de se connecter à un compte; pour se faire, il ne doit pas être déjà
            //connecté à un compte
            //la console demande à l'utilisateur de saisir son identifiant et son mot de passe, et si il n'y a pas d'erreur
            //le message "Connecté avec succès" est affiché
            void SeConnecter(GestionaireDeComptes g)
            {
                if (!g.IsSomeoneConnected)
                {
                    Console.Write("ID :");
                    string id = Console.ReadLine();
                    Console.Write("Mot de passe :");
                    string mdp = Console.ReadLine();
                    Compte tmp = new Compte(id, mdp);
                    if (g.ItemsComptes.Exists(tmp))
                    {
                        int indexCompte = g.ItemsComptes.LesItems.IndexOf(tmp);
                        g.ItemsComptes.ItemCourant = g.ItemsComptes.LesItems[indexCompte] as Compte;
                        Console.BackgroundColor = ConsoleColor.Green;
                        Console.ForegroundColor = ConsoleColor.Black;
                        Console.WriteLine("Connecté avec succès");
                        Console.BackgroundColor = ConsoleColor.Black;
                        Console.ForegroundColor = ConsoleColor.White;
                    }
                    else
                    {
                        Console.BackgroundColor = ConsoleColor.Red;
                        Console.ForegroundColor = ConsoleColor.Black;
                        Console.WriteLine("ID ou mot de passe incorecte");
                        Console.BackgroundColor = ConsoleColor.Black;
                        Console.ForegroundColor = ConsoleColor.White;
                    }
                }
                else
                {
                    Console.WriteLine("Vous êtes déja connecté");
                }
            }

            //cette méthode sert à ajouter un langage à la liste des favoris ; pour se faire, l'utilisateur doit être 
            //connecté, et avoir préalablement selectionné un langage (sinon un message d'erreur est affiché)
            //si le langge est dejà dans les favoris, le message "Le langage -nom- est deja dans votre liste des favoris" 
            //est affiché, sinon le langage est ajouté aux favorix et le message "Le langage -nom- a été ajouté avec succès 
            //dans la liste des favoris" est affiché
            void AjouterUnLangageEnFavoris(GestionaireDeLangages gl, GestionaireDeComptes gc)
            {
                if(gc.IsSomeoneConnected)
                {
                    if(gl.IsASelectedLanguage)
                    {
                        if (!(gc.ItemsComptes.ItemCourant as Compte).LesLangagesFavoris.Contains((gl.ItemsLangages.ItemCourant as Langage)))
                        {
                            (gc.ItemsComptes.ItemCourant as Compte).LesLangagesFavoris.Add((gl.ItemsLangages.ItemCourant as Langage));
                            Console.WriteLine($"Le langage {(gl.ItemsLangages.ItemCourant as Langage).Nom} a été ajouté avec succès dans la liste des favoris");
                        }
                        else
                        {
                            Console.WriteLine($"Le langage {(gl.ItemsLangages.ItemCourant as Langage).Nom} est deja dans votre liste des favoris");
                        }
                    }
                    else
                    {
                        Console.WriteLine("Vous n'avez séléctioné aucun langage");
                    }
                }
                else
                {
                    Console.WriteLine("Vous n'êtes pas connécté");
                }
            }

            //cette méthode sert à selectionné un langage; la liste des langages est affiché (triée par noms)
            //la console demande à l'utilisateur de saisir le langage à selectionner
            //si le langage est dans la liste, le message "Langage séléctioné : -nom-" est affiché, et le langage est 
            //selectionné, sinon le message "Le langage -nom- n'existe pas." est affiché
            void SelectionerUnLangage(GestionaireDeLangages g)
            {
                ValeurTri ancieneValeurTri = g.ItemsLangages.Filtre;
                g.ItemsLangages.Filtre = ValeurTri.Nom;
                foreach(Langage lang in g.ItemsLangages.LesItems)
                {
                    Console.WriteLine(lang.Nom);
                }
                Console.Write("Saisir le nom du langage :");
                string rép = Console.ReadLine();
                foreach(Langage lang in g.ItemsLangages.LesItems)
                {
                    if (lang.Nom == rép)
                    {
                        g.ItemsLangages.ItemCourant = lang;
                        g.ItemsLangages.Filtre = ancieneValeurTri;
                        Console.WriteLine($"Langage séléctioné :\n{lang}");
                        return;
                    }
                }
                Console.WriteLine($"Le langage {rép} n'existe pas.");
                g.ItemsLangages.Filtre = ancieneValeurTri;
            }

            //cette méthode sert à supprimer un langage de la liste des favoris ; pour se faire, l'utilisateur doit être 
            //connecté, et avoir préalablement selectionné un langage (sinon un message d'erreur est affiché)
            //si le langge n'est pas dans les favoris, le message "Ce langage n'est pas dans votre liste de favoris" 
            //est affiché, sinon le langage est supprimé des favoris et le message "Le langage -nom- a bien été retiré de 
            //la liste des favoris" est affiché
            void SupprimerUnLangageDesFavoris(GestionaireDeLangages gl, GestionaireDeComptes gc)
            {
                if(gc.IsSomeoneConnected)
                {
                    if (gl.IsASelectedLanguage)
                    {
                        if ((gc.ItemsComptes.ItemCourant as Compte).LesLangagesFavoris.Remove((gl.ItemsLangages.ItemCourant as Langage)))
                        {
                            Console.WriteLine($"Le langage {(gl.ItemsLangages.ItemCourant as Langage).Nom} a bien été retiré de la liste des favoris");
                        }
                        else
                        {
                            Console.WriteLine($"Ce langage n'est pas dans votre liste de favoris");
                        }
                    }
                    else
                    {
                        Console.WriteLine("Aucun langage n'est séléctioné");
                    }
                }
                else
                {
                    Console.WriteLine("Vous n'êtes pas connécté");
                }
            }

            //cette méthode permet d'afficher les details sur le langage selectionné (son nom, sa date de création,
            //ses auteurs ...); pour se faire, l'utilisateur doit avoir préalablement selectionné un langage 
            //(sinon un message d'erreur s'affiche)
            void AfficherLeLangageSéléctioné(GestionaireDeLangages g)
            {
                if(g.IsASelectedLanguage)
                {
                    Console.WriteLine(g.ItemsLangages.ItemCourant);
                }
                else
                {
                    Console.WriteLine("Aucun langage n'est séléctioné");
                }
            }

            //cette méthode permet à l'utilisateur de se déconnecter; pour se faire, l'utilisateur doit etre connecté 
            //à un commpte (sinon un message d'erreur d'affiche)
            //si il n'y a pas d'erreur, le message "Déconnecté avec succès" est affiché, et le compte connecté devient null
            void SeDeconnecter(GestionaireDeComptes g)
            {
                if (g.IsSomeoneConnected)
                {
                    g.ItemsComptes.ItemCourant = null;
                    Console.BackgroundColor = ConsoleColor.Green;
                    Console.ForegroundColor = ConsoleColor.Black;
                    Console.WriteLine("Déconnecté avec succès");
                    Console.BackgroundColor = ConsoleColor.Black;
                    Console.ForegroundColor = ConsoleColor.White;
                }
                else
                {
                    Console.WriteLine("Vous n'êtes pas connecté");
                }
            }

            //cette boucle va permettre à l'utilisateur de tester toutes les fonctionnalités ; tant que l'utilisateur 
            //n'a pas saisi ctrl+c pour quitter la boucle, un menu s'affiche et la console demande à l'utilisateur
            //de choisir une action en saisissant le numero correspondant
            while (l)
            {

                Console.WriteLine("1\tAfficher les langages");
                Console.WriteLine("2\tAfficher les comptes");
                Console.WriteLine("3\tSe connecter");
                Console.WriteLine("4\tCréer un compte");
                Console.WriteLine("5\tModifier le mot de passe du compte");
                Console.WriteLine("6\tSupprimer un commpte");
                Console.WriteLine("7\tSéléctioner un langage");
                Console.WriteLine("8\tAfficher le langage séléctionné");
                Console.WriteLine("9\tAjouter un langage");
                Console.WriteLine("10\tModifier le langage séléctionné");
                Console.WriteLine("11\tSupprimer le langage sélectionné");
                Console.WriteLine("12\tAjouter le langage séléctioné en favoris");
                Console.WriteLine("13\tSupprimé le langage séléctioné des favoris");
                Console.WriteLine("14\tSe déconnecter");
                Console.Write("Choix : ");
                string réponse = Console.ReadLine();
                switch (réponse)
                {
                    case "1":
                        AfficherLesLangages(gestionaireDeLanagage as GestionaireDeLangages, gestionaireDeComptes as GestionaireDeComptes);
                        break;
                    case "2":
                        AfficherLesComptes(gestionaireDeComptes as GestionaireDeComptes);
                        break;
                    case "3":
                        SeConnecter(gestionaireDeComptes as GestionaireDeComptes);
                        break;
                    case "4":
                        CreerUnCompte(gestionaireDeComptes as GestionaireDeComptes);
                        break;
                    case "5":
                        ModifierCompte(gestionaireDeComptes as GestionaireDeComptes);
                        break;
                    case "6":
                        SupprimerUnCompte(gestionaireDeComptes as GestionaireDeComptes);
                        break;
                    case "7":
                        SelectionerUnLangage(gestionaireDeLanagage as GestionaireDeLangages);
                        break;
                    case "8":
                        AfficherLeLangageSéléctioné(gestionaireDeLanagage as GestionaireDeLangages);
                        break;
                    case "9":
                        AjouterUnLangage(gestionaireDeLanagage as GestionaireDeLangages, gestionaireDeComptes as GestionaireDeComptes);
                        break;
                    case "10":
                        ModifierLangageSelectionné(gestionaireDeLanagage as GestionaireDeLangages, gestionaireDeComptes as GestionaireDeComptes);
                        break;
                    case "11":
                        SupprimerLangageSelectionné(gestionaireDeLanagage as GestionaireDeLangages, gestionaireDeComptes as GestionaireDeComptes);
                        break;
                    case "12":
                        AjouterUnLangageEnFavoris(gestionaireDeLanagage as GestionaireDeLangages, gestionaireDeComptes as GestionaireDeComptes);
                        break;
                    case "13":
                        SupprimerUnLangageDesFavoris(gestionaireDeLanagage as GestionaireDeLangages, gestionaireDeComptes as GestionaireDeComptes);
                        break;
                    case "14":
                        SeDeconnecter(gestionaireDeComptes as GestionaireDeComptes);
                        break;
                    default:
                        Debug.WriteLine("Bye");
                        l = false;
                        break;
                }
            }
        }
    }
}
