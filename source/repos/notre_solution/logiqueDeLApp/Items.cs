﻿using System;
using System.Collections.Generic;
using System.Text;

namespace logiqueDeLApp
{
    public class Items
    {
        public Item ItemCourant { get; private set; }
        public IList<Item> LesItems { get; private set; }

        public Items(IList<Item> items)
        {
            LesItems = items;
        }

        public Items(Item itemCourant, IList<Item> lesItems)
        {
            ItemCourant = itemCourant;
            LesItems = lesItems;
        }
        public Items()
        {
            LesItems = new List<Item>();
        }

        public void Supprimer(Item it)
        {
            if(LesItems.Remove(it) == false)
            {
                // lever une exception qui dit que it n'existe pas 
            }
        }

        public void Ajouter(Item it)
        {
            if (LesItems.Contains(it) == true)
            {
                // lever un exeption qui dit que lesItems contien it
                return;
            }
            LesItems.Add(it);
        }

    }
}
