﻿using System;
using System.Collections.Generic;
using System.Text;

namespace logiqueDeLApp
{
    public class Langage : Item
    {
        public string Nom { get; private set; }
        public DateTime DateDuLangage { get; private set; }
        public IList<string> LesAuteurs { get; private set; }
        public string Doucumentation { get; private set; }
        public string chemainDuLogo { get; private set;  }
        public IList<string> logicielsConus { get; private set; }
        public string utilitéDuLangage { get; private set; }

    }
}
