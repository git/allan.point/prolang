﻿using System;
using System.Collections.Generic;
using System.Text;

namespace logiqueDeLApp
{
    public class Compte
    {
        public string Identifiant { get; private set; }
        private string MotDePasse { get;  set; }
        public bool EstAdmin { get; private set; }
        public bool EstConnecter { get; set;  }
    }
}
