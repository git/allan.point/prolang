﻿using System;
using System.Collections.Generic;

namespace logiqueDeLApp
{
    class Paradigme
    {
        public string Nom { get; private set; }
        public IList<Paradigme> LesParadigmes { get; private set; }

        public Paradigme(string nom, List<Paradigme> lesParadigmes)
        {
            Nom = nom;
            LesParadigmes = lesParadigmes;
        }

        public bool IsParadigmeExists(Paradigme p)
        {
            return LesParadigmes.Contains(p);
        }
    }
}
