﻿using notre_bibliotheque;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace vues
{
    /// <summary>
    /// Logique d'interaction pour FormulaireCompte.xaml
    /// cette fenetre est utiliser pour créer un compte
    /// </summary>
    public partial class FormulaireCompte : Window
    {
        public FormulaireCompte()
        {
            InitializeComponent();
        }

        /// <summary>
        ///si l'utilisateur clique sur le bouton ok pour valider, et que un ou plusieurs champs ne sont pas remplis,
        ///un message d'erreur est affiché, et si un des champs n'est pas rempli correctement, une exception est levée
        ///et un message d'erreur s'affiche 
        ///quand l'utilisateur a créé son compte, il sera automatiquement connecté; ce de faite, son identifiant sera
        ///affiché en haut à droite dans mainwindow
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Valider_Click(object sender, RoutedEventArgs e)
        {
            if (String.IsNullOrEmpty(idDuCompteSaisi.Text.Trim()) || String.IsNullOrEmpty(mdpSaisi.Password.Trim())
                || String.IsNullOrEmpty(mdpSaisi2.Password.Trim()))
            {
                MessageBox.Show("Vous devez remplir tous les champs", "compte", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else
            {
                GestionaireDeComptes g = ((App.Current as App).MainWindow as MainWindow).GestionaireCompte;
                try
                {
                    Compte tmp = g.VerfierCreationCompte(idDuCompteSaisi.Text, mdpSaisi.Password, mdpSaisi2.Password, adminOui.IsChecked.Value);
                    g.ItemsComptes.Ajouter(tmp);
                    g.ItemsComptes.ItemCourant = tmp;
                    if (tmp.EstAdmin)
                    {
                        (App.Current.MainWindow as MainWindow).boutonLangages.IsEnabled = true;
                    }
                    else
                    {
                        (App.Current.MainWindow as MainWindow).boutonLangages.IsEnabled = false;
                    }
                    (App.Current.MainWindow as MainWindow).temoinID.DataContext = g.ItemsComptes.ItemCourant;
                    Close();
                }
                catch (Exception erreur)
                {
                    MessageBox.Show(erreur.Message, "créer un compte", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            
        }

        /// <summary>
        /// si l'utilisateur clique sur le bouton annuler, la page se ferme
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Annuler_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
