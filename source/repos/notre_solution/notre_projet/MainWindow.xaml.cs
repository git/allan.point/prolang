﻿using données;
using notre_bibliotheque;
using PersDataContract;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace vues
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private GestionaireDeComptes gestionaireDeComptes;
        private GestionaireDeLangages gestionaireDeLangages;
        public GestionaireDeLangages GestionaireLangages => gestionaireDeLangages;
        public GestionaireDeComptes GestionaireCompte => gestionaireDeComptes;
        private Navigateur navigateur;
        private IPercistance percistance;

        public MainWindow()
        {
            InitializeComponent();
            // Si le chargement par DataContract ne fonctione pas, c'est un Stub qui est choisi à la place
            percistance = new MyDataContract("Persistance.xml");
            Gestionaire.Persistance = percistance;
            gestionaireDeComptes = new GestionaireDeComptes();
            gestionaireDeLangages = new GestionaireDeLangages();
            try
            {
                gestionaireDeLangages.ChargerLesLangages();
                gestionaireDeComptes.ChargerLesComptes();
            }catch(Exception)
            {
                Gestionaire.Persistance = new Stub("Persistance.xml");
                Debug.WriteLine("Imposible de charger le data contract");
                gestionaireDeLangages.ChargerLesLangages();
                gestionaireDeComptes.ChargerLesComptes();
            }
            gestionaireDeComptes.ItemsComptes.ItemsDeLAutreType = gestionaireDeLangages.ItemsLangages;
            navigateur = new Navigateur(gestionaireDeLangages);
            gestionaireDeLangages.ItemsLangages.ItemsDeLAutreType = gestionaireDeComptes.ItemsComptes;
            DataContext = gestionaireDeLangages;
            ModifierLangageMenu.DataContext = gestionaireDeLangages;
            SupprimerLangageMenu.DataContext = gestionaireDeLangages;
        }

        /// <summary>
        /// Permet d'ouvrir une fenêtre modale qui permet de creer un langage
        /// </summary>
        /// <param name="sender">Objet qui appelle NouveauLangage_Click</param>
        /// <param name="e">Argument de l'evenement</param>
        private void NouveauLangage_Click(object sender, RoutedEventArgs e)
        {
            FormulaireLangage formWin = new FormulaireLangage(true);
            formWin.ShowDialog();
        }

        /// <summary>
        /// Permet d'ouvrir une fenêtre modale qui permet de modifier un langage
        /// </summary>
        /// <param name="sender">Objet qui appelle ModifierLangage_Click</param>
        /// <param name="e"></param>
        private void ModifierLangage_Click(object sender, RoutedEventArgs e)
        {
            FormulaireLangage formWin = new FormulaireLangage(false);
            formWin.DataContext = gestionaireDeLangages.ItemsLangages.ItemCourant;
            formWin.ShowDialog();
        }

        /// <summary>
        /// Pemrmet de Supprimer un Langage de la liste des langages disponibles
        /// </summary>
        /// <param name="sender">Objet qui appelle SupprimerLangage_Click</param>
        /// <param name="e">Arguments de l'evenement</param>
        private void SupprimerLangage_Click(object sender, RoutedEventArgs e)
        {
            switch (MessageBox.Show($"Etes-vous sûr de supprimer {(gestionaireDeLangages.ItemsLangages.ItemCourant as Langage).Nom} ?", "supprimer", MessageBoxButton.OKCancel, MessageBoxImage.Warning))
            {
                case MessageBoxResult.OK:
                    gestionaireDeLangages.ItemsLangages.Supprimer(gestionaireDeLangages.ItemsLangages.ItemCourant);
                    break;
                default:
                case MessageBoxResult.Cancel:
                    break;
            }

        }

        /// <summary>
        /// Permet de changer d'UC si besion et ajoute le Langage séléctioné à l'historique de l'utilisateur si il est connécté
        /// </summary>
        /// <param name="sender">Objet qui appelle MasterDesLangages_SelectionChanged</param>
        /// <param name="e"></param>
        private void MasterDesLangages_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            detail.Content = navigateur.ControlCourant;
            if (gestionaireDeComptes.IsSomeoneConnected && (sender as ListBox).SelectedItem != null)
            {
                (gestionaireDeComptes.ItemsComptes.ItemCourant as Compte).AjouterUnLangageALHistorique((sender as ListBox).SelectedItem as Langage);
            }
            GestionaireLangages.OnPropertyChange("IsASelectedLanguage");
            Debug.WriteLine(GestionaireLangages.ItemsLangages.ItemCourant);
        }

        /// <summary>
        /// Permet d'affichier l'UC de connexion
        /// </summary>
        /// <param name="sender">Objet appellant SeConnecter_Click</param>
        /// <param name="e">Argument de l'evenement</param>
        private void SeConnecter_Click(object sender, RoutedEventArgs e)
        {
            GestionaireLangages.ItemsLangages.ItemCourant = null;
            detail.Content = navigateur.ControlCourant;
        }

        /// <summary>
        /// Permet de déconnecter l'utilisateur courrant
        /// </summary>
        /// <param name="sender">Objet qui appelle Deconnexion_Click</param>
        /// <param name="e">Argument de l'evenement</param>
        private void Deconnexion_Click(object sender, RoutedEventArgs e)
        {
            if (gestionaireDeComptes.IsSomeoneConnected)
            {
                gestionaireDeComptes.ItemsComptes.ItemCourant = null;
                gestionaireDeLangages.ItemsLangages.ItemCourant = null;
                temoinID.DataContext = "Non connecté";
                detail.Content = navigateur.ControlCourant;
                GestionaireCompte.onPropertyChanged("IsSomeoneConnected");
                boutonLangages.IsEnabled = false;
            }
            else
            {
                MessageBox.Show("Vous n'êtes pas connecté", "déconnexion", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        /// <summary>
        /// Permet de sauvegarder les données de l'application
        /// </summary>
        /// <param name="sender">Objet qui appelle SauvegarderALaFermeture</param>
        /// <param name="e">Argument de l'evenement</param>
        private void SauvegarderALaFermeture(object sender, EventArgs e)
        {
            Gestionaire.Persistance = new MyDataContract("Persistance.xml");
            List<Langage> langagesÀSauvegarder = new List<Langage>();
            List<Compte> comptesÀSauvegarder = new List<Compte>();
            GestionaireLangages.ItemsLangages.Filtre = ValeurTri.Nom;
            foreach(Item it in GestionaireLangages.ItemsLangages.LesItems)
            {
                langagesÀSauvegarder.Add(it as Langage);
            }
            foreach(Item it in GestionaireCompte.ItemsComptes.LesItems)
            {
                comptesÀSauvegarder.Add(it as Compte);
            }
            Gestionaire.Persistance?.SauvegarderLesDonnées(langagesÀSauvegarder, comptesÀSauvegarder);
        }
    }
}
