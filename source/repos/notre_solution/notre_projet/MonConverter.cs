﻿using notre_bibliotheque;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Windows.Data;
using System.Windows.Media.Imaging;

namespace vues
{
    /// <summary>
    /// cette classe sert de converteur du type boolean au type bitmapimage; elle implémente l'interface IValueConverter
    /// ce converteur est utilisé par la propriété Source de l'image d'un bouton dans DescriptionLangageUC.xaml
    /// </summary>
    public class MonConverter : IValueConverter
    {
        GestionaireDeComptes gc = ((App.Current as App).MainWindow as MainWindow).GestionaireCompte;
        GestionaireDeLangages gl = ((App.Current as App).MainWindow as MainWindow).GestionaireLangages;

        /// <summary>
        /// //le converteur rend une bitmapimage en fonction de la valeur de la propriété DansLesFavoris de Langage
        /// si le langage est dans les favoris du compte actuellement connecté, la source de l'image sera étoile_jaune.png,
        /// sinon la source sera étoile_noire.png
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture )
        {
            BitmapImage bi = new BitmapImage();
            if (gc.IsSomeoneConnected)
            {
                if ((gl.ItemsLangages.ItemCourant as Langage).DansLesFavoris)
                {
                    bi.BeginInit();
                    bi.UriSource = new Uri("/images/étoile_jaune.png", UriKind.RelativeOrAbsolute);
                    bi.EndInit();
                }
                else
                {
                    bi.BeginInit();
                    bi.UriSource = new Uri("/images/étoile_noire.png", UriKind.RelativeOrAbsolute);
                    bi.EndInit();
                }
            }
            else
            {
                bi.BeginInit();
                bi.UriSource = new Uri("/images/étoile_noire.png", UriKind.RelativeOrAbsolute);
                bi.EndInit();
            }
            return bi;
        }

        /// <summary>
        /// cette méthode ne sera pas utilisée
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns>object</returns>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
