﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using notre_bibliotheque;

namespace vues
{
    /// <summary>
    ///     Permmet d'afficher un langage
    /// </summary>
    public partial class DescriptionLangageUC : UserControl
    {
        public GestionaireDeLangages gLangages = ((App.Current as App).MainWindow as MainWindow).GestionaireLangages;
        public GestionaireDeComptes gComptes = ((App.Current as App).MainWindow as MainWindow).GestionaireCompte;
        public DescriptionLangageUC()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Permet d'ajouter un langage dans la liste des favoris
        /// </summary>
        /// <param name="sender">Objet appelant Favoris_Click</param>
        /// <param name="e">Argument de l'evenement</param>
        private void Favoris_Click(object sender, RoutedEventArgs e)
        {
            if (!gComptes.IsSomeoneConnected)
            {
                MessageBox.Show("impossible d'ajouter le langage aux favoris : vous n'êtes pas connecté", "favoris", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else
            { 
                if ((gLangages.ItemsLangages.ItemCourant as Langage).DansLesFavoris == false)
                {
                    (gLangages.ItemsLangages.ItemCourant as Langage).DansLesFavoris = true;
                    (gComptes.ItemsComptes.ItemCourant as Compte).AjouterUnLangageAuxFavoirs(gLangages.ItemsLangages.ItemCourant as Langage);
                }
                else
                {
                    (gLangages.ItemsLangages.ItemCourant as Langage).DansLesFavoris = false;
                    (gComptes.ItemsComptes.ItemCourant as Compte).RetirerUnLangageDesFavoris((gLangages.ItemsLangages.ItemCourant as Langage));

                }
            }
            
        }
    }
}
