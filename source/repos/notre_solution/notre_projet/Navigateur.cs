﻿using notre_bibliotheque;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Windows.Controls;

namespace vues
{
    /// <summary>
    /// Permet de naviguer entre les User Controls
    /// </summary>
    class Navigateur
    {
        private GestionaireDeLangages gestionaireDeLangages;
        // Propriété calcullé qui revoi le bon UC en fonction de l'êtat de l'application
        public UserControl ControlCourant
        {
            get
            {
                //Si un langage est selectioné on l'affiche, sinon on affiche l'UC par défaut.
                if(gestionaireDeLangages.IsASelectedLanguage)
                {
                    return new DescriptionLangageUC();
                }
                return new ConnexionUC();
            }
        }

        /// <summary>
        /// Constructeur de Navigateur
        /// </summary>
        /// <param name="gestionaireDeLangages">Référance du gestionaire de langage</param>
        public Navigateur(GestionaireDeLangages gestionaireDeLangages)
        {
            this.gestionaireDeLangages = gestionaireDeLangages;
        }

    }
}
