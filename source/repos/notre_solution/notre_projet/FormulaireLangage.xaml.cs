﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using notre_bibliotheque;
using System.IO;
using System.Diagnostics;

namespace vues
{
    /// <summary>
    /// Logique d'interaction pour FormulaireLangage.xaml
    /// cette fenetre est utilisé pour ajouter un langage mais également pour modifier un langage selectionné
    /// </summary>
    public partial class FormulaireLangage : Window
    {
        public GestionaireDeLangages g = ((App.Current as App).MainWindow as MainWindow).GestionaireLangages;
        private Langage nouveauLangage;
        private bool IsForAdd { get; set; }
        public FormulaireLangage(bool isForAdd)
        {
            // si il s'agit d'un ajout, la propriété IsForAdd a pour valeur true, et les champs sont préremplis par des
            // "valeurs inconnues"
            // s'il s'agit d'une modification, la propriété IsForAdd a pour valeur false, et les champs sont préremplus 
            // avec les informations du langage à modifier
            
            //s'il s'agit d'un ajout, l'attibut nouveauLangage de type Langage sera le nouveau Langage à ajouter, 
            //sinon, le langage courant à modifier sera affecté à cet attribut
            InitializeComponent();
            if(isForAdd)
            {
                    nouveauLangage = new Langage("Nom Inconnu",
                    DateTime.Now.Year,
                    new List<string>() { "Auteur Inconnu", },
                    "documentation inconnu",
                    ".",
                    "Hello World",
                    new List<string>() { "logiciel inconnu", },
                    "Utilité inconnu",
                    new List<string>() { "Paradigme inconnu", },
                    1);

            }
            else
            {
                nouveauLangage = g.ItemsLangages.ItemCourant as Langage;
                Nom.IsEnabled = false;
            }
            DataContext = nouveauLangage;
            IsForAdd = isForAdd;
        }
 
        /// <summary>
        /// quand l'utilisateur clique sur le bouton browsefile, il peut parcourir ses fichiers et selectionner une image
        /// qui fera office de logo pour le langage (seuls les images de type .jpg .jpeg .png .bmp pourront
        /// être selectionnées)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BrowseFile_Click(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog fileDialog = new Microsoft.Win32.OpenFileDialog();
            fileDialog.InitialDirectory = @"C:\Users";
            fileDialog.FileName = "Images";
            fileDialog.DefaultExt = ".jpg | .jpeg | .png | .bmp";
            fileDialog.Filter = "Image files (.jpg, .jpeg, .png, .bmp)  | *.jpg; *.jpeg; *.png; *.bpm";

            bool? result = fileDialog.ShowDialog();

            if(result == true)
            {
                string fdFileName = fileDialog.FileName;
                string fileName = System.IO.Path.GetFileName(fdFileName);
                string newImage = $"../images_logo/{fileName}";
                Debug.WriteLine(Directory.GetCurrentDirectory());
                try
                {
                    File.Copy(fdFileName, newImage);
                }catch(IOException except)
                {
                    Debug.WriteLine(except.Message);
                }
                nouveauLangage.CheminDuLogo = fileName;
                Logo.Source = new BitmapImage(new Uri(newImage, UriKind.Relative));
            }
        }

        /// <summary>
        /// si l'utilisateur clique sur le bouton ok pour valider, et que un ou plusieurs champs ne sont pas remplis, 
        /// un message d'erreur est affiché, et si un des champs n'est pas rempli correctement, une exception est levée
        /// et un message d'erreur s'affiche
        /// si il s'agit d'un ajout, le nouveau langage est ajouté, et si il s'agit d'un modification
        /// les informations du langage sont set
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ValiderNouveauLangage_Click(object sender, RoutedEventArgs e)
        {
            if (String.IsNullOrEmpty(Nom.Text.Trim()) || String.IsNullOrEmpty(Date.Text.Trim()) || String.IsNullOrEmpty(Doc.Text.Trim())
                || String.IsNullOrEmpty(Exe.Text.Trim()) || String.IsNullOrEmpty(Gen.Text.Trim()) ||
                String.IsNullOrEmpty(Utilité.Text.Trim()) || string.IsNullOrEmpty(Logiciel.Text.Trim())
                || String.IsNullOrEmpty(auteurs.Text.Trim())|| string.IsNullOrEmpty(Paradigmes.Text.Trim()))
            {
                MessageBox.Show("Vous devez remplir tous les champs", "langage", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else 
            {
                try
                {
                    List<string> auteursTmp = new List<string>();
                    List<string> paradigmesTmp = new List<string>();
                    List<string> logicielTmp = new List<string>();
                    foreach (string aut in auteurs.Text.Split(','))
                    {
                        if(string.IsNullOrEmpty(aut.Trim()))
                        {
                            MessageBox.Show("Nom d'auteur invalide", "langage", MessageBoxButton.OK, MessageBoxImage.Error);
                            return;
                        }
                        auteursTmp.Add(aut);
                    }
                    foreach (string parad in Paradigmes.Text.Split(','))
                    {
                        if(string.IsNullOrEmpty(parad.Trim()))
                        {
                            MessageBox.Show("Paradigmes invalide", "langage", MessageBoxButton.OK, MessageBoxImage.Error);
                            return;
                        }
                        paradigmesTmp.Add(parad);
                    }
                    foreach (string logiciel in Logiciel.Text.Split(','))
                    {
                        if(string.IsNullOrEmpty(logiciel.Trim()))
                        {
                            MessageBox.Show("Logiciel invalide", "langage", MessageBoxButton.OK, MessageBoxImage.Error);
                            return;
                        }
                        logicielTmp.Add(logiciel);
                    }
                    nouveauLangage.Documentation = Doc.Text;
                    nouveauLangage.ExempleDeCode = Exe.Text;
                    nouveauLangage.Nom = Nom.Text;
                    nouveauLangage.UtilitéDuLangage = Utilité.Text;
                    nouveauLangage.LesAuteurs = auteursTmp;
                    nouveauLangage.LesParadigmes = paradigmesTmp;
                    nouveauLangage.LogicielsConus = logicielTmp;
                    nouveauLangage.DateDuLangage = int.Parse(Date.Text);
                    nouveauLangage.Generation = int.Parse(Gen.Text);
 //                   nouveauLangage.CheminDuLogo = Logo.Source.ToString();
                }
                catch (Exception err)
                {
                    MessageBox.Show(err.Message, "langage", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                if (!IsForAdd)
                {
                    g.ItemsLangages.Supprimer(nouveauLangage);
                }
                g.ItemsLangages.Ajouter(nouveauLangage);
                g.ItemsLangages.ItemCourant = nouveauLangage;
                this.Close();

            }
            
        }

        /// <summary>
        /// si l'utilisateur clique sur le bouton annuler, la page se ferme
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Quit_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
