﻿using notre_bibliotheque;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using données;
using System.ComponentModel;

namespace vues
{
    /// <summary>
    /// Permet la connexion à un compte depuis la vue
    /// </summary>
    public partial class ConnexionUC : UserControl
    {
        
        public ConnexionUC()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Méthode appelé lors ce l'utilisateur clique sur le bouton "Créer Un Compte"
        /// Elle ouvre une nouvelle fenêtre qui permet de créer un compte
        /// </summary>
        /// <param name="sender">Objet appelant CreerCompte_Click</param>
        /// <param name="e">Argument de l'evenement</param>
        private void CreerCompte_Click(object sender, RoutedEventArgs e)
        {
            FormulaireCompte form = new FormulaireCompte();
            form.ShowDialog();
        }

        /// <summary>
        /// Permet de se connecter lors du clique su le bouton "Se Connecter"
        /// </summary>
        /// <param name="sender">Objet appellant ConnexionAUnCompte_Click</param>
        /// <param name="e">Argument de l'évenement</param>
        /// <remarks>La connexion est faite lors ce que ItemsCompte du gestionaire de comptes est mutté (null => new Compte)</remarks>
        private void ConnexionAUnCompte_Click(object sender, RoutedEventArgs e)
        {
            GestionaireDeComptes g = ((App.Current as App).MainWindow as MainWindow).GestionaireCompte;
            if (g.IsSomeoneConnected)
            {
                MessageBox.Show("Vous êtes déjà connecté", "connexion", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else
            {
                if (String.IsNullOrEmpty(idDuCompteSaisi.Text.Trim()) || String.IsNullOrEmpty(mdpSaisi.Password.Trim()))
                {
                    MessageBox.Show("Vous devez remplir les champs", "connexion", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                else
                {
                    Compte tmp = new Compte(idDuCompteSaisi.Text, mdpSaisi.Password);
                    MenuItem boutonLangage = (App.Current.MainWindow as MainWindow).boutonLangages;
                    if (g.ItemsComptes.LesItems.Contains(tmp))
                    {
                        int indexCompte = g.ItemsComptes.LesItems.IndexOf(tmp);
                        g.ItemsComptes.ItemCourant = g.ItemsComptes.LesItems[indexCompte] as Compte;
                        (App.Current.MainWindow as MainWindow).temoinID.DataContext = g.ItemsComptes.ItemCourant;
                        boutonLangage.DataContext = g.ItemsComptes.ItemCourant;
                        if ((g.ItemsComptes.ItemCourant as Compte).EstAdmin)
                        {
                            (App.Current.MainWindow as MainWindow).boutonLangages.IsEnabled = true;
                        }
                        else
                        {
                            (App.Current.MainWindow as MainWindow).boutonLangages.IsEnabled = false;
                        }
                    }
                    else
                    {
                        MessageBox.Show("L'identifiant ou le mot de passe est incorrecte", "connexion", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
            }
           
        }
    }
}
