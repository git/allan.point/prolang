﻿using notre_bibliotheque;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace PersDataContract
{
    /// <summary>
    /// Permet de réunir les données à sérializer pour les écrire dans un seul fichier et ne pas avoir de pproblème de référence
    /// </summary>
    [DataContract]
    class DonnésÀSerialiser
    {
        [DataMember]
        public IEnumerable<Langage> LangagesASauvegarder { get; private set; }

        [DataMember]
        public IEnumerable<Compte> ComptesASauvegarder { get; private set; }

        public DonnésÀSerialiser(IEnumerable<Langage> lesLangages, IEnumerable<Compte> lesComptes)
        {
            LangagesASauvegarder = lesLangages;
            ComptesASauvegarder = lesComptes;
        }
    }
}
