﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using notre_bibliotheque;

namespace PersDataContract
{
    /// <summary>
    /// Permet de sauvegarder/charger dans/depuis un fichier xml grâce au DataContract
    /// </summary>
    public class MyDataContract : IPercistance
    {
        public string FilePath { get; set; } = Path.Combine(Directory.GetCurrentDirectory(), "Pers");
        private string fileName;
        private string FileNameComplet => Path.Combine(FilePath, fileName);

        /// <summary>
        /// Constructeur de MyDataContract
        /// </summary>
        /// <param name="fileName">Nom de fichier de sauvegarde/chargement</param>
        /// <exception cref="ArgumentException">Lors ce que le fichier n'existe pas, une excepetion est lancée</exception>
        public MyDataContract(string fileName)
        {
            this.fileName = fileName;
        }
        /// <summary>
        /// Permet de charger les donner depuis un fichier xml
        /// </summary>
        /// <returns>Un dictionaire avec une clef "Langages" et "Comptes" qui contiennent réspectivement des la liste des langages chargés et la liste des comptes chargés</returns>
        public Dictionary<string, IEnumerable<Item>> ChargerLesDonnées()
        {
            if (!File.Exists(FileNameComplet))
            {
                throw new ArgumentException("Le fichier spécifié n'existe pas");
            }
            Dictionary<string, IEnumerable<Item>> DictionaireÀRetourner = new Dictionary<string, IEnumerable<Item>>();
            DonnésÀSerialiser tmp;

            // Ce DataContractSerializer permet de lire un objet de type DonnésÀSerialiser
            DataContractSerializer serializer = new DataContractSerializer(typeof(DonnésÀSerialiser));
            using(Stream fichierDeChargement = File.OpenRead(FileNameComplet))
            {
                tmp = serializer.ReadObject(fichierDeChargement) as DonnésÀSerialiser;
            }
            DictionaireÀRetourner.Add("Langages", tmp.LangagesASauvegarder);
            DictionaireÀRetourner.Add("Comptes", tmp.ComptesASauvegarder);
            return DictionaireÀRetourner;
        }

        /// <summary>
        /// Permet de sauvegarder les données de l'application
        /// </summary>
        /// <param name="langagesASauvgarder">Liste des langages disponible dans l'application</param>
        /// <param name="comptesÀSauvegarder">Liste des comptes de l'application</param>
        public void SauvegarderLesDonnées(IEnumerable<Langage> langagesASauvgarder, IEnumerable<Compte> comptesÀSauvegarder)
        {
            if (!Directory.Exists(FilePath))
            {
                Directory.CreateDirectory(FilePath);
            }

            //Ce DataContractSerializer permet d'ecire les données de l'application dans un fichier xml tout en préservant les référances
            DataContractSerializer serializer = new DataContractSerializer(typeof(DonnésÀSerialiser), new DataContractSerializerSettings() { PreserveObjectReferences = true });
            DonnésÀSerialiser tmp = new DonnésÀSerialiser(langagesASauvgarder, comptesÀSauvegarder);
            using (Stream fichierDeSauvgarde = File.Create(FileNameComplet))
            {
                serializer.WriteObject(fichierDeSauvgarde, tmp);
            }
        }
    }
}
