﻿using System;
using notre_bibliotheque;
using System.Text;
using System.Collections.Generic;
using System.Diagnostics;
using Données;
using System.Linq;

namespace test_number_one
{
    class Program
    {
        private static Chargeur loader = new Stub("");
        static void Main(string[] args)
        {
            TestGlobal();
        }

        static void TestGlobal()
        {

            IList<string> lesAuteurs = new List<string>();
            lesAuteurs.Add("auteur 1");
            lesAuteurs.Add("auteur 2");
            IList<string> lesLogiciels = new List<string>();
            lesLogiciels.Add("logiciel 1");
            lesLogiciels.Add("logiciel 2");
            lesLogiciels.Add("logiciel 3");           


            IList<Item> lesLangages = loader.ChargerLesLangages();
            Items itemsLang = new Items(lesLangages);


            IList<Item> lesComptes = loader.ChargerLesComptes();
            Item it11 = new Compte("id13", "mdp1");
            lesComptes.Add(it11);
            Gestionaire gestionaireDeComptes = loader.ChargerGestionaireDeCompte();
            Gestionaire gestionaireDeLanagage = loader.ChargerGestionaireDeLangage();





            bool l = true;

            void AfficherLesLangages(GestionaireDeLangages g)
            {
                Console.WriteLine("Comment Afficher les langages");
                Console.WriteLine("1. Trier par nom");
                Console.WriteLine("2. Afficher selement les favoris");
                Console.WriteLine("3. Trier par ordre de date de création");
                Console.WriteLine("4. Trier par ordre de génération");


                string rép = Console.ReadLine();
                switch (rép)
                {
                    case "2":
                        if(GestionaireDeComptes.CompteCourant is null)
                        {
                            Console.WriteLine("Persone n'est connecté");
                            return;
                        }
                        if(GestionaireDeComptes.CompteCourant.LesLangagesFavoris.Count == 0)
                        {
                            Console.WriteLine($"{GestionaireDeComptes.CompteCourant.Identifiant} n'a pas de langage favoris");
                            return;
                        }
                        g.ItemsLangages.Filtre = ValeurTri.Favoris;
                        break;
                    case "3":
                        g.ItemsLangages.Filtre = ValeurTri.Date;
                        break;
                    case "4":
                        g.ItemsLangages.Filtre = ValeurTri.Génération;
                        break;
                    case "1":
                    default:
                        g.ItemsLangages.Filtre = ValeurTri.Nom;
                        break;
                }
                foreach (Item it in g.ItemsLangages.LesItems)
                {
                    Console.WriteLine($"{it}\n**************************************************************************");
                }
            }
            void AfficherLesComptes(GestionaireDeComptes g)
            {
                foreach (Item it in g.ItemsComptes.LesItems)
                {
                    Console.WriteLine($"{it}\n**************************************************************************");
                }
            }
            void CreerUnCompte(GestionaireDeComptes g)
            {
                Console.Write("ID :");
                string id = Console.ReadLine();
                Console.Write("Mot de passe :");
                string mdp1 = Console.ReadLine();
                Console.Write("Mot de passe :");
                string mdp2 = Console.ReadLine();
                Console.Write("Accès au mode administrateur ? (o/n) : ");
                string rep = Console.ReadLine();
                bool admin;
                if (rep == "o")
                {
                    admin = true;
                }
                else admin = false;
                try
                {
                    Compte tmp = g.VerfierCreationCompte(id, mdp1, mdp2,admin);
                    g.ItemsComptes.Ajouter(tmp);
                    GestionaireDeComptes.CompteCourant = tmp;
                    Console.WriteLine("Votre compte à bien été créé");
                }catch(Exception e)
                {
                    Console.WriteLine(e.Message);
                }

            }
            void SupprimerUnCompte(GestionaireDeComptes g)
            {
                Console.Write("ID :");
                string id = Console.ReadLine();
                Console.Write("MDP :");
                string mdp = Console.ReadLine();
                Compte tmp = new Compte(id, mdp);
                try
                {
                    g.ItemsComptes.Supprimer(tmp);
                    Console.WriteLine($"{tmp.Identifiant} à bien était supprimé");
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }

            }         
            void AjouterUnLangage(GestionaireDeLangages g)
            {
                if (GestionaireDeComptes.IsSomeoneConnected)
                {
                    if (GestionaireDeComptes.CompteCourant.EstAdmin)
                    {
                        bool success = false;
                        string respuesta;
                        string nom = "";
                        string aut = "";
                        string doc = "";
                        string logo = "";
                        string log = "";
                        string utilite = "";
                        int gen = 0;
                        DateTime date = DateTime.Now;
                        while (!success) 
                        {
                            Console.Write("Nom : ");
                            nom = Console.ReadLine();
                            if(string.IsNullOrEmpty(nom.Trim()))
                            {
                                Console.WriteLine("Le nom doit être saisi");
                                continue;
                            }
                            success = true;
                        }
                        IList<string> auteurs = new List<string>();
                        success = false;
                        while (!success)
                        {
                            do
                            {
                                Console.Write("Un Auteur : ");
                                aut = Console.ReadLine();
                                auteurs.Add(aut);
                                Console.Write("D'auteurs auteurs ? (o/n) : ");
                                respuesta = Console.ReadLine();
                            } while (respuesta != "n");
                            if(auteurs.Count == 0)
                            {
                                Console.WriteLine("Il doit y avoir au moins un auteur");
                                continue;
                            }
                            success = true;
                        }

                        success = false;
                        while (!success)
                        {
                            Console.Write("Date de création (jj/mm/aa) : ");
                            try
                            {
                                date = DateTime.Parse(Console.ReadLine());
                                success = true;
                            }
                            catch (Exception e)
                            {
                                Console.WriteLine(e.Message);
                            }
                        }

                        success = false;
                        while (!success)
                        {
                            Console.Write("Lien vers la documentation : ");
                            doc = Console.ReadLine();
                            if(String.IsNullOrEmpty(doc.Trim()))
                            {
                                Console.WriteLine("Le lien vers la documentation doit être rempli");
                                continue;
                            }
                            success = true;
                        }

                        success = false;
                        while (!success)
                        {
                            Console.Write("Logo (chemin) : ");

                            logo = Console.ReadLine();
                            if(string.IsNullOrEmpty(logo.Trim()))
                            {
                                Console.WriteLine("Le chemin doit être saisi");
                                continue;
                            }
                            success = true;
                        }

                        IList<string> logiciels = new List<string>();
                        success = false;
                        while (!success)
                        {
                            do
                            {
                                Console.Write("Un logiciel : ");
                                log = Console.ReadLine();
                                logiciels.Add(log);
                                Console.Write("D'auteurs logiciels ? (o/n) : ");
                                respuesta = Console.ReadLine();
                            } while (respuesta != "n");
                            if(logiciels.Count == 0)
                            {
                                Console.WriteLine("Il faut saisir des logiciels");
                                continue;
                            }
                            success = true;
                        }

                        bool finish = false;
                        string exemple = "";
                        success = false;
                        while (!success)
                        {
                            Console.WriteLine("Exemple de code (Hello World) - tapez entrer pour ecrire une nouvelle ligne");
                            while (!finish)
                            {
                                exemple += $"{Console.ReadLine()}\n";
                                Console.Write("Ecrire une autre ligne ? (o/n) : ");
                                respuesta = Console.ReadLine();
                                if (respuesta == "n")
                                {
                                    finish = true;
                                }
                            }
                            if(String.IsNullOrEmpty(exemple.Trim()))
                            {
                                Console.WriteLine("L'exemple doit être saisi");
                                continue;
                            }
                            success = true;
                        }

                        success = false;
                        while (!success)
                        {
                            Console.Write("Utilités du langage : ");

                            utilite = Console.ReadLine();
                            if(string.IsNullOrEmpty(utilite))
                            {
                                Console.WriteLine("Il faut saisir l'utilité du langage");
                                continue;
                            }
                            success = true;

                        }

                        IList<Paradigme> paradigmes = new List<Paradigme>();
                        Paradigme parag;
                        success = false;
                        while (!success)
                        {
                            do
                            {
                                Console.Write("Un paradigme : ");
                                parag = new Paradigme(Console.ReadLine());
                                paradigmes.Add(parag);
                                Console.Write("D'auteurs paradigmes ? (o/n) : ");
                                respuesta = Console.ReadLine();
                            } while (respuesta != "n");

                            if(paradigmes.Count == 0)
                            {
                                Console.WriteLine("Il doit y avoir au moins un paradigme");
                                continue;
                            }
                            success = true;
                        }
                        success = false;
                        while(!success)
                        {
                            Console.Write("Définire une génération :");
                            try
                            {
                                gen = int.Parse(Console.ReadLine());
                                if(gen <= 0)
                                {
                                    Console.WriteLine("La génération du langage doit être superieur à 0");
                                    continue;
                                }
                            }catch(System.IO.IOException e)
                            {
                                Console.WriteLine(e.Message);
                            }
                            success = true;
                        }
                        Langage lang = new Langage(nom, date, auteurs, doc, logo, exemple, logiciels, utilite, paradigmes, gen);
                        g.ItemsLangages.Ajouter(lang);
                        if (g.ItemsLangages.Exists(lang))
                        {
                            Console.WriteLine("Le Langage a bien été ajouté à la liste");
                            Console.WriteLine("*****************************************");
                            Console.WriteLine(lang);
                        }
                        else
                        {
                            Console.WriteLine("pb : le langage n'a pas été ajouté");
                        }
                    }
                    else
                    {
                        Console.WriteLine("Vous n'avez pas accès au mode adminstrateur");
                    }
                }
                else
                {
                    Console.WriteLine("Vous n'êtes pas connécté");
                }
            }
            void ModifierLangageSelectionné()
            {
                if (GestionaireDeComptes.IsSomeoneConnected)
                {
                    if (GestionaireDeComptes.CompteCourant.EstAdmin)
                    {
                        if (GestionaireDeLangages.IsASelectedLanguage)
                        {
                            bool success = false;
                            Console.WriteLine(GestionaireDeLangages.LangageCourant);
                            string answer;
                            Console.Write("Modifier le nom ? (o/n) : ");
                            answer = Console.ReadLine();
                            if(answer == "o")
                            {
                                while (!success)
                                {
                                    Console.Write("Nouveau nom : ");
                                    try
                                    {
                                        GestionaireDeLangages.LangageCourant.Nom = Console.ReadLine();
                                        success = true;
                                    }
                                    catch (Exception e)
                                    {
                                        Console.WriteLine(e.Message);
                                    }
                                }
                                success = false;
                            }
                            Console.Write("Modifier la date de création ? (o/n) : ");
                            answer = Console.ReadLine();
                            if(answer == "o")
                            {
                                while (!success)
                                {
                                    Console.Write("jj/mm/aa : ");
                                    try
                                    {
                                        GestionaireDeLangages.LangageCourant.DateDuLangage = DateTime.Parse(Console.ReadLine());
                                        success = true;
                                    }
                                    catch (Exception e)
                                    {
                                        Console.WriteLine(e.Message);
                                    }
                                }
                                success = false;
                            }
                            Console.Write("Modifier les auteurs ? (o/n) : ");
                            answer = Console.ReadLine();
                            if(answer == "o")
                            {
                                IList<string> auteurs = new List<string>();
                                string aut;
                                string reponse;
                                while (!success)
                                {
                                    do
                                    {
                                        Console.Write("Un Auteur : ");
                                        aut = Console.ReadLine();
                                        auteurs.Add(aut);
                                        Console.Write("D'auteurs auteurs ? (o/n) : ");
                                        reponse = Console.ReadLine();
                                    } while (reponse != "n");
                                    try
                                    {
                                        GestionaireDeLangages.LangageCourant.LesAuteurs = auteurs;
                                        success = true;
                                    }
                                    catch(Exception e)
                                    {
                                        Console.WriteLine(e.Message);
                                    }
                                }
                                success = false;
                            }
                            Console.Write("Modifier le logo ? (o/n) : ");
                            answer = Console.ReadLine();
                            if(answer == "o")
                            {
                                while (!success)
                                {
                                    Console.Write("Nouveau chemin du logo : ");
                                    try
                                    {
                                        GestionaireDeLangages.LangageCourant.CheminDuLogo = Console.ReadLine();
                                        success = true;
                                    }
                                    catch (Exception e)
                                    {
                                        Console.WriteLine(e.Message);
                                    }
                                }
                                success = false;
                            }
                            Console.Write("Modifier la documentation ? (o/n) : ");
                            answer = Console.ReadLine();
                            if(answer == "o")
                            {
                                while (!success)
                                {
                                    Console.Write("Nouveau lien vers la documentation : ");
                                    try
                                    {
                                        GestionaireDeLangages.LangageCourant.Documentation = Console.ReadLine();
                                        success = true;
                                    }
                                    catch(Exception e)
                                    {
                                        Console.WriteLine(e.Message);
                                    }
                                }
                                success = false;
                            }
                            Console.Write("Modifier les logiciels ? (o/n) : ");
                            answer = Console.ReadLine();
                            if(answer == "o")
                            {
                                IList<string> logiciels = new List<string>();
                                string log;
                                string reponse;
                                while (!success)
                                {
                                    do
                                    {
                                        Console.Write("Un logiciel : ");
                                        log = Console.ReadLine();
                                       logiciels.Add(log);
                                        Console.Write("D'auteurs logiciels ? (o/n) : ");
                                        reponse = Console.ReadLine();
                                    } while (reponse != "n");
                                    try
                                    {
                                        GestionaireDeLangages.LangageCourant.LogicielsConus = logiciels;
                                        success = true;
                                    }
                                    catch(Exception e)
                                    {
                                        Console.WriteLine(e.Message);
                                    }
                                }
                                success = false;
                            }
                            Console.Write("Modifier l'utilité ? (o/n) : ");
                            answer = Console.ReadLine();
                            if(answer == "o")
                            {
                                while (!success)
                                {
                                    Console.Write("Nouvelle utilité : ");
                                    try
                                    {
                                        GestionaireDeLangages.LangageCourant.UtilitéDuLangage = Console.ReadLine();
                                        success = true;
                                    }
                                    catch (Exception e)
                                    {
                                        Console.WriteLine(e.Message);
                                    }
                                }
                                success = false;
                            }
                            Console.Write("Modifier l'exemple ? (o/n) : ");
                            answer = Console.ReadLine();
                            if (answer == "o")
                            {
                                bool finish = false;
                                string exemple = "";
                                while (!success)
                                {
                                    while (!finish)
                                    {
                                        exemple += $"{Console.ReadLine()}\n";
                                        Console.Write("Ecrire une autre ligne ? (o/n) : ");
                                        answer = Console.ReadLine();
                                        if (answer == "n")
                                        {
                                            finish = true;
                                        }
                                    }
                                    try
                                    {
                                        GestionaireDeLangages.LangageCourant.ExempleDeCode = exemple;
                                        success = true;
                                    }
                                    catch (Exception e)
                                    {
                                        Console.WriteLine(e.Message);
                                    }
                                }
                                success = false;
                            }
                            Console.Write("Modifier les paradigmes ? (o/n) : ");
                            answer = Console.ReadLine();
                            if(answer == "o")
                            {
                                IList<Paradigme> paradigmes = new List<Paradigme>();
                                Paradigme parag;
                                string reponse;
                                while (!success)
                                {
                                    do
                                    {
                                        Console.Write("Un paradigme : ");
                                        parag = new Paradigme(Console.ReadLine());
                                        paradigmes.Add(parag);
                                        Console.Write("D'auteurs paradigmes ? (o/n) : ");
                                        reponse = Console.ReadLine();
                                    } while (reponse != "n");
                                    try
                                    {
                                        GestionaireDeLangages.LangageCourant.LesParadigmes = paradigmes;
                                        success = true;
                                    }
                                    catch (Exception e)
                                    {
                                        Console.WriteLine(e.Message);
                                    }
                                }
                                success = false;
                            }
                        }
                        else
                        {
                            Console.WriteLine("Vous n'avez séléctioné aucun langage");
                        }
                    }
                    else
                    {
                        Console.WriteLine("Vous n'avez pas accès au mode adminstrateur");
                    }
                }
                else
                {
                    Console.WriteLine("Vous n'êtes pas connécté");
                }
            }
            void SupprimerLangageSelectionné(GestionaireDeLangages g)
            {
                if (GestionaireDeComptes.IsSomeoneConnected)
                {
                    if (GestionaireDeComptes.CompteCourant.EstAdmin)
                    {
                        if (GestionaireDeLangages.IsASelectedLanguage)
                        {
                            try
                            {
                                Console.WriteLine(GestionaireDeLangages.LangageCourant);
                                Console.Write("Supprimer ce langage ? (o/n) : ");
                                string answer = Console.ReadLine();
                                if (answer == "o")
                                {
                                    g.ItemsLangages.Supprimer(GestionaireDeLangages.LangageCourant); 
                                    if (g.ItemsLangages.Exists(GestionaireDeLangages.LangageCourant))
                                    {
                                        Console.WriteLine("pb : le langage n'a pas été ajouté");
                                    }
                                    else
                                    {
                                        Console.WriteLine($"Le langage à bien était supprimé");
                                        GestionaireDeLangages.LangageCourant = null;
                                    }
                                }
                            }
                            catch (Exception e)
                            {
                                Console.WriteLine(e.Message);
                            }
                        }
                        else
                        {
                            Console.WriteLine("Vous n'avez séléctioné aucun langage");
                        }
                    }
                    else
                    {
                        Console.WriteLine("Vous n'avez pas accès au mode administrateur");
                    }
                }
                else
                {
                    Console.WriteLine("Vous n'êtes pas connécté");
                }

            }
            void ModifierCompte() 
            {
                if (GestionaireDeComptes.IsSomeoneConnected)
                {
                    Console.WriteLine("Le compte courant : ");
                    Console.WriteLine(GestionaireDeComptes.CompteCourant);
                    Console.WriteLine("**********************************");
                    Console.Write("Nouveau mot de passe : ");
                    string mdp = Console.ReadLine();
                    Console.Write("Confirmer le mot de passe : ");
                    string mdp2 = Console.ReadLine();
                    while (mdp != mdp2)
                    {
                        Console.WriteLine("Les deux mots de passe ne sont pas les mêmes; veuillez réessayer");
                        Console.Write("Nouveau mot de passe : ");
                        mdp = Console.ReadLine();
                        Console.Write("Confirmer le mot de passe : ");
                        mdp2 = Console.ReadLine();
                    }
                    GestionaireDeComptes.CompteCourant.MotDePasse = mdp;
                    Console.WriteLine("Le compte courant : ");
                    Console.WriteLine(GestionaireDeComptes.CompteCourant);
                    Console.WriteLine("**********************************");
                    Console.WriteLine("Mot de passe modifié avec succès");
                }
                else
                {
                    Console.WriteLine("Vous n'êtes pas connecté");
                }
                
            }
            void SeConnecter(GestionaireDeComptes g)
            {
                if (!GestionaireDeComptes.IsSomeoneConnected)
                {
                    Console.Write("ID :");
                    string id = Console.ReadLine();
                    Console.Write("Mot de passe :");
                    string mdp = Console.ReadLine();
                    Compte tmp = new Compte(id, mdp);
                    if (g.ItemsComptes.Exists(tmp))
                    {
                        int indexCompte = g.ItemsComptes.LesItems.IndexOf(tmp);
                        GestionaireDeComptes.CompteCourant = g.ItemsComptes.LesItems[indexCompte] as Compte;
                        Console.BackgroundColor = ConsoleColor.Green;
                        Console.ForegroundColor = ConsoleColor.Black;
                        Console.WriteLine("Connecté avec succès");
                        Console.BackgroundColor = ConsoleColor.Black;
                        Console.ForegroundColor = ConsoleColor.White;
                    }
                    else
                    {
                        Console.BackgroundColor = ConsoleColor.Red;
                        Console.ForegroundColor = ConsoleColor.Black;
                        Console.WriteLine("ID ou mot de passe incorecte");
                        Console.BackgroundColor = ConsoleColor.Black;
                        Console.ForegroundColor = ConsoleColor.White;
                    }
                }
                else
                {
                    Console.WriteLine("Vous êtes déja connecté");
                }
            }
            void AjouterUnLangageEnFavoris()
            {
                if(GestionaireDeComptes.IsSomeoneConnected)
                {
                    if(GestionaireDeLangages.IsASelectedLanguage)
                    {
                        if (!GestionaireDeComptes.CompteCourant.LesLangagesFavoris.Contains(GestionaireDeLangages.LangageCourant))
                        {
                            GestionaireDeComptes.CompteCourant.LesLangagesFavoris.Add(GestionaireDeLangages.LangageCourant);
                            Console.WriteLine($"Le langage {GestionaireDeLangages.LangageCourant.Nom} a été ajouté avec succès dans la liste des favoris");
                        }
                        else
                        {
                            Console.WriteLine($"Le langage {GestionaireDeLangages.LangageCourant.Nom} est deja dans votre liste des favoris");
                        }
                    }
                    else
                    {
                        Console.WriteLine("Vous n'avez séléctioné aucun langage");
                    }
                }
                else
                {
                    Console.WriteLine("Vous n'êtes pas connécté");
                }
            }
            void SelectionerUnLangage(GestionaireDeLangages g)
            {
                ValeurTri ancieneValeurTri = g.ItemsLangages.Filtre;
                g.ItemsLangages.Filtre = ValeurTri.Nom;
                foreach(Langage lang in g.ItemsLangages.LesItems)
                {
                    Console.WriteLine(lang.Nom);
                }
                Console.Write("Saisir le nom du langage :");
                string rép = Console.ReadLine();
                foreach(Langage lang in g.ItemsLangages.LesItems)
                {
                    if (lang.Nom == rép)
                    {
                        GestionaireDeLangages.LangageCourant = lang;
                        g.ItemsLangages.Filtre = ancieneValeurTri;
                        Console.WriteLine($"Lanagage séléctioné :\n{lang}");
                        return;
                    }
                }
                Console.WriteLine($"Le langage {rép} n'existe pas.");
                g.ItemsLangages.Filtre = ancieneValeurTri;
            }
            void SupprimerUnLangageDesFavoris()
            {
                if(GestionaireDeComptes.IsSomeoneConnected)
                {
                    if (GestionaireDeLangages.IsASelectedLanguage)
                    {
                        if (GestionaireDeComptes.CompteCourant.LesLangagesFavoris.Remove(GestionaireDeLangages.LangageCourant))
                        {
                            Console.WriteLine($"Le langage {GestionaireDeLangages.LangageCourant.Nom} a bien été retiré de la liste des langages");
                        }
                        else
                        {
                            Console.WriteLine($"Ce langage n'est pas dans votre liste de favoris");
                        }
                    }
                    else
                    {
                        Console.WriteLine("Aucun langage n'est séléctioné");
                    }
                }
                else
                {
                    Console.WriteLine("Vous n'êtes pas connécté");
                }
            }
            void AfficherLeLangageSéléctioné()
            {
                if(GestionaireDeLangages.IsASelectedLanguage)
                {
                    Console.WriteLine(GestionaireDeLangages.LangageCourant);
                }
                else
                {
                    Console.WriteLine("Aucun langage n'est séléctioné");
                }
            }
            void SeDeconnecter(GestionaireDeComptes g)
            {
                if (GestionaireDeComptes.IsSomeoneConnected)
                {
                    GestionaireDeComptes.CompteCourant.EstConnecter = false;
                    GestionaireDeComptes.CompteCourant = null;
                    Console.BackgroundColor = ConsoleColor.Green;
                    Console.ForegroundColor = ConsoleColor.Black;
                    Console.WriteLine("Déconnecté avec succès");
                    Console.BackgroundColor = ConsoleColor.Black;
                    Console.ForegroundColor = ConsoleColor.White;
                }
                else
                {
                    Console.WriteLine("Vous n'êtes pas connecté");
                }
            }
            while (l)
            {

                Console.WriteLine("1\tAfficher les langages");
                Console.WriteLine("2\tAfficher les comptes");
                Console.WriteLine("3\tCréer un compte");
                Console.WriteLine("4\tSupprimer un commpte");
                Console.WriteLine("5\tAjouter un langage");
                Console.WriteLine("6\tSupprimer le langage sélectionné");
                Console.WriteLine("7\tSe connecter");
                Console.WriteLine("8\tModifier le mot de passe du compte");
                Console.WriteLine("9\tSéléctioner un langage");
                Console.WriteLine("10\tAjouter le langage séléctioné en favoris");
                Console.WriteLine("11\tSupprimé le langage séléctioné des favoris");
                Console.WriteLine("12\tAfficher le langage séléctioné");
                Console.WriteLine("13\tModifier le langage séléctioné");
                Console.WriteLine("14\tSe déconnecter");
                Console.Write("Choix : ");
                string réponse = Console.ReadLine();
                switch (réponse)
                {
                    case "1":
                        AfficherLesLangages(gestionaireDeLanagage as GestionaireDeLangages);
                        break;
                    case "2":
                        AfficherLesComptes(gestionaireDeComptes as GestionaireDeComptes);
                        break;
                    case "3":
                        CreerUnCompte(gestionaireDeComptes as GestionaireDeComptes);
                        break;
                    case "4":
                        SupprimerUnCompte(gestionaireDeComptes as GestionaireDeComptes);
                        break;
                    case "5":
                        AjouterUnLangage(gestionaireDeLanagage as GestionaireDeLangages);
                        break;
                    case "6":
                        SupprimerLangageSelectionné(gestionaireDeLanagage as GestionaireDeLangages);
                        break;
                    case "7":
                        SeConnecter(gestionaireDeComptes as GestionaireDeComptes);
                        break;
                    case "8":
                        ModifierCompte();
                        break;
                    case "9":
                        SelectionerUnLangage(gestionaireDeLanagage as GestionaireDeLangages);
                        break;
                    case "10":
                        AjouterUnLangageEnFavoris();
                        break;
                    case "11":
                        SupprimerUnLangageDesFavoris();
                        break;
                    case "12":
                        AfficherLeLangageSéléctioné();
                        break;
                    case "13":
                        ModifierLangageSelectionné();
                        break;
                    case "14":
                        SeDeconnecter(gestionaireDeComptes as GestionaireDeComptes);
                        break;
                    default:
                        Debug.WriteLine("Bye");
                        l = false;
                        break;
                }
            }
        }
    }
}
